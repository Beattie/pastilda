#include <thread.hpp>
#include <ticks.hpp>
#include <critical.hpp>
#include <queue.hpp>

#include <config/FreeRTOSConfig.h>
#include <hw/System.hpp>
#include <hw/kbd/Package.hpp>
#include <tasks/LedBlinkingTask.h>
#include <tasks/UsbDeviceTask.h>
#include <tasks/UsbHostTask.h>
#include <tasks/process-network/SwitcherTask.h>
#include <tasks/process-network/LogicTask.h>
#include <tasks/WatchdogTask.h>

namespace rtos = cpp_freertos;

void* operator new(size_t sz) {
    return pvPortMalloc(sz);
}

void* operator new[](size_t sz) {
    return pvPortMalloc(sz);
}

void operator delete(void* p) {
    vPortFree(p);
}

void operator delete[](void* p) {
    vPortFree(p);
}

extern "C" {
    void* __dso_handle;
    void* _fini;
}

int main()
{
#ifdef PASTILDA_SHIFTED
	SCB->VTOR = 0x8008000;
#	warning "Vectors table will be relocated"
#else
#	warning "Vectors table will not be relocated"
#endif

	hw::system::clock_init();
	hw::system::systick_init();

#if ( configUSE_TRACE_FACILITY == 1 )
    vTraceEnable(TRC_START);
#endif

	hw::system::irq_disable(); // it will be enabled by Scheduler

	auto led_blinking_task = new tasks::LedBlinkingTask();
	auto usb_device_task = new tasks::UsbDeviceTask();
	auto usb_host_task = new tasks::UsbHostTask();
	auto switcher_task = new tasks::SwitcherTask();
	auto logic_task = new tasks::LogicTask();
	auto output_task = new tasks::OutputManagerTask();
	auto db_task = new tasks::DatabaseTask();
	//auto watchdog_task = new tasks::WatchdogTask();

	rtos::Thread::StartScheduler();
	while(true);

	return 0;
}

void vAssertCalled(const char* pcFile, unsigned long ulLine)
{
    rtos::CriticalSection::Enter();
    {
        while(true)
        {
            portNOP();
        }
    }
    rtos::CriticalSection::Exit();
}

void vApplicationMallocFailedHook()
{
    rtos::CriticalSection::Enter();
    {
        while(true)
        {
            portNOP();
        }
    }
    rtos::CriticalSection::Exit();
}
