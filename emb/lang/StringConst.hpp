/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANG_STRINGCONST_HPP_
#define LANG_STRINGCONST_HPP_

namespace lang {

class StringConst
{
public:
	template<std::size_t N>
	constexpr StringConst(const char (&s)[N]) :
		_ptr(s), _size(N - 1)
	{ }

	constexpr const char& operator[](std::size_t i) {
		return _ptr[i];
	}

	constexpr const char* data() {
		return _ptr;
	}

	constexpr std::size_t length() {
		return _size;
	}

private:
	const char* const _ptr;
	const std::size_t _size;
};


}  // namespace lang

#endif /* LANG_STRINGCONST_HPP_ */
