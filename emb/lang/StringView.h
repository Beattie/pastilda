/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANG_STRINGVIEW_H_
#define LANG_STRINGVIEW_H_

#include <experimental/string_view>
#include <etl/hash.h>

namespace lang {

using StringView = std::experimental::string_view;

}

namespace etl {

#if ETL_8BIT_SUPPORT
  template <>
  struct hash<lang::StringView>
  {
    size_t operator()(const lang::StringView& text) const
    {
      return etl::__private_hash__::generic_hash<>(
              reinterpret_cast<const uint8_t*>(&text[0]),
              reinterpret_cast<const uint8_t*>(&text[text.size()]));
    }
  };
#endif

}


#endif /* LANG_STRINGVIEW_H_ */
