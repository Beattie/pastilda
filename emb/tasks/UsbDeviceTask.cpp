/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "UsbDeviceTask.h"

namespace tasks {

    static UsbDeviceTask *device_task;

    UsbDeviceTask::UsbDeviceTask() :
		                Thread(config::UsbDeviceTask::NAME,
		                       config::UsbDeviceTask::STACK_SIZE,
		                       config::UsbDeviceTask::PRIORITY)
    {
        device_task = this;

        _input_queue = new rtos::Queue(10, hw::kbd::PACKAGE_LENGTH);
        semaphore_interrupt = new rtos::BinarySemaphore(false);

        this->Start();
    }

    void UsbDeviceTask::enqueue(hw::kbd::Package package)
    {
    	device_task->_input_queue->Enqueue(&package);
    }

    void UsbDeviceTask::Run()
    {
        usbd_register_set_config_callback(usb.device, USB_set_config_callback);
        usb.msd_init();
        usb.enable_interrupt();
        TickType_t remote_wakeup_delay =
              rtos::Ticks::MsToTicks(hw::usb::UsbDevice::REMOTE_WAKEUP_DELAY_MS);
        while (true)
        {
            if(semaphore_interrupt->Take(portMAX_DELAY))
            {
            	if (_dequeue() == Action::SEND)
                {
            		if(usb.is_host_suspend()) {
            			if( _package != hw::kbd::ZERO_PACKAGE ) {
            				usb.remote_wakeup(true);
            				Thread::Delay(remote_wakeup_delay);
            				usb.remote_wakeup(false);
            			}
            			else {
            				semaphore_interrupt->Give();
            				continue;
            			}
            		}
            		//TODO: [USB] Why _package is not local?
                    device_task->usb_send_packet_nonblock(&_package);
                }
            }
        }
    }

    int UsbDeviceTask::hid_control_request(usbd_device *usbd_dev, struct usb_setup_data *req, uint8_t **buf, uint16_t *len,
                                           void (**complete)(usbd_device *usbd_dev, struct usb_setup_data *req))
    {
        (void)complete;
        (void)usbd_dev;

        if ((req->bmRequestType & USB_REQ_TYPE_DIRECTION) == USB_REQ_TYPE_IN)
        {
            if ((req->bmRequestType & USB_REQ_TYPE_TYPE) == USB_REQ_TYPE_STANDARD)
            {
                if (req->bRequest == USB_REQ_GET_DESCRIPTOR)
                {
                    if (req->wValue == 0x2200)
                    {
                        *buf = (uint8_t *)hw::usb::UsbDevice::keyboard_report_descriptor;
                        *len = sizeof(hw::usb::UsbDevice::keyboard_report_descriptor);
                        return (USBD_REQ_HANDLED);
                    }
                    else if (req->wValue == 0x2100)
                    {
                        *buf = (uint8_t *)&hw::usb::UsbDevice::keyboard_hid_function;
                        *len = sizeof(hw::usb::UsbDevice::keyboard_hid_function);
                        return (USBD_REQ_HANDLED);
                    }
                    return (USBD_REQ_NOTSUPP);
                }
            }
            else if ((req->bmRequestType & USB_REQ_TYPE_TYPE) == USB_REQ_TYPE_CLASS)
            {
                if (req->bRequest == hw::usb::HidRequest::GET_REPORT)
                {
                    *buf = (uint8_t*)&hw::kbd::ZERO_PACKAGE;
                    *len = hw::kbd::PACKAGE_LENGTH;

                    return (USBD_REQ_HANDLED);
                }
                else if (req->bRequest == hw::usb::HidRequest::GET_IDLE)
                {
                    *buf = &_keyboard_idle;
                    *len = sizeof(_keyboard_idle);
                    return (USBD_REQ_HANDLED);
                }
                else if (req->bRequest == hw::usb::HidRequest::GET_PROTOCOL)
                {
                    *buf = &_keyboard_protocol;
                    *len = sizeof(_keyboard_protocol);
                    return (USBD_REQ_HANDLED);
                }
                return (USBD_REQ_NOTSUPP);
            }
        }

        else
        {
            if ((req->bmRequestType & USB_REQ_TYPE_TYPE) == USB_REQ_TYPE_CLASS)
            {
                if (req->bRequest == hw::usb::HidRequest::SET_REPORT)
                {
                    if (*len == 1)
                    {
                    	msg::SwitcherData switcher_data;
                    	switcher_data.leds = **buf;

                    	SwitcherTask::MsgType message;
                    	message.token = SwitcherTask::MsgType::Token::LEDS_FROM_DEVICE;
                    	message.data = switcher_data;
                    	SwitcherTask::fifo_push_from_isr(&message);
                    }
                    return (USBD_REQ_HANDLED);
                }
                else if (req->bRequest == hw::usb::HidRequest::SET_IDLE)
                {
                    _keyboard_idle = req->wValue >> 8;
                    return (USBD_REQ_HANDLED);
                }
                else if (req->bRequest == hw::usb::HidRequest::SET_PROTOCOL)
                {
                    _keyboard_protocol = req->wValue;
                    return (USBD_REQ_HANDLED);
                }
            }
            return (USBD_REQ_NOTSUPP);
        }

        return (USBD_REQ_NEXT_CALLBACK);
    }

    void UsbDeviceTask::hid_set_config(usbd_device *usbd_dev, uint16_t wValue)
    {
        (void)wValue;
        (void)usbd_dev;

        usbd_ep_setup(usbd_dev, hw::usb::Endpoint::E_KEYBOARD, USB_ENDPOINT_ATTR_INTERRUPT, 8, _set_semaphore_interrupt);
        device_task->_set_semaphore_interrupt(0,0);
        usbd_register_control_callback(usbd_dev, USB_REQ_TYPE_INTERFACE, USB_REQ_TYPE_RECIPIENT, USB_control_callback );
    }

    void UsbDeviceTask::_set_semaphore_interrupt(usbd_device* dev, unsigned char)
    {
        BaseType_t pxHigherPriorityTaskWoken = pdFALSE;
        device_task->semaphore_interrupt->GiveFromISR(&pxHigherPriorityTaskWoken);
        if( pxHigherPriorityTaskWoken == pdTRUE )
            Thread::Yield();

    }

    auto UsbDeviceTask::_dequeue() -> Action
    {
    	if (_input_queue->Dequeue(&_package, portMAX_DELAY))
    		return Action::SEND;

    	return Action::NONE;
    }

    int USB_control_callback(usbd_device *usbd_dev,
                             struct usb_setup_data *req, uint8_t **buf, uint16_t *len,
                             usbd_control_complete_callback *complete)
    {
        return( device_task->hid_control_request(usbd_dev, req, buf, len, complete));
    }


    void USB_set_config_callback(usbd_device *usbd_dev,
                                 uint16_t wValue)
    {
        device_task->hid_set_config(usbd_dev, wValue) ;
    }

    void USB_OTG_IRQ()
    {
        usbd_poll(device_task->usb.device);
        device_task->last_usb_request_time = rtos::Ticks::TicksToMs(rtos::Ticks::GetTicks());
    }

    uint16_t UsbDeviceTask::usb_send_packet_nonblock(hw::kbd::Package *package)
    {
        return usbd_ep_write_packet(device_task->usb.device, 0x81, package, hw::kbd::PACKAGE_LENGTH);
    }

} /* namespace tasks */
