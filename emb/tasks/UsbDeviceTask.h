/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TASKS_USBDEVICE_H_
#define TASKS_USBDEVICE_H_

#include <thread.hpp>
#include <ticks.hpp>
#include <critical.hpp>
#include <queue.hpp>
#include <semaphore.hpp>

#include <config/TasksConfig.h>
#include <hw/UsbDevice.h>
#include <hw/kbd/Package.hpp>

#include "process-network/SwitcherTask.h"

namespace tasks {

namespace rtos = cpp_freertos;

#define USB_OTG_IRQ                  otg_fs_isr
extern "C" void USB_OTG_IRQ();

class UsbDeviceTask final : rtos::Thread
{
public:
	hw::usb::UsbDevice usb;
	volatile uint32_t last_usb_request_time;
	rtos::BinarySemaphore *semaphore_interrupt;

	~UsbDeviceTask() = default;
    UsbDeviceTask();

    static void enqueue(hw::kbd::Package package);
	int hid_control_request(usbd_device *usbd_dev, struct usb_setup_data *req,
			                uint8_t **buf, uint16_t *len,
							void (**complete)(usbd_device *usbd_dev, struct usb_setup_data *req));

	void hid_set_config(usbd_device *usbd_dev, uint16_t wValue);
	uint16_t usb_send_packet_nonblock(hw::kbd::Package *package );

private:
	enum Action {
		SEND,
		NONE
	};

	uint8_t _keyboard_protocol = 1;
	uint8_t _keyboard_idle = 0;
	rtos::Queue *_input_queue;
	hw::kbd::Package _package;

	void Run();
	static void _set_semaphore_interrupt(usbd_device* dev, unsigned char);
	Action _dequeue();
};

int USB_control_callback(usbd_device *usbd_dev, struct usb_setup_data *req,
                         uint8_t **buf, uint16_t *len,
						 usbd_control_complete_callback *complete);

void USB_set_config_callback(usbd_device *usbd_dev, uint16_t wValue);

} /* namespace tasks */

#endif /* TASKS_USBDEVICE_H_ */
