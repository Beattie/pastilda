/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TASKS_DATABASETASK_H_
#define TASKS_DATABASETASK_H_

#include <lang/StringView.h>
#include <config/TasksConfig.h>

#include "common/Message.hpp"
#include "common/IKahnProcessTask.hpp"

namespace tasks {

namespace msg {

enum DatabaseCommand : uint8_t {
	DECRYPT,
	DB_CLOSE,
	DECRYPT_PASS,
};

using DatabasePassphrase = lang::StringView;
using DatabaseMessage = common::Message<DatabaseCommand, DatabasePassphrase>;

} /* namespace msg */


class DatabaseTask final :
	public common::IKahnProcessTask<config::DatabaseTask::NAME,
	                                msg::DatabaseMessage,
								    config::DatabaseTask::FIFO_SIZE>
{
public:
	DatabaseTask();
	virtual ~DatabaseTask() = default;

	void Run();

private:
    enum State {
        IDLE,
        DECRYPT,
        OPENED,
    };

    enum StateAction {
        STAY,
        NEXT,
        FAIL,
        QUIT,
    };

	class DatabaseManager;

	DatabaseManager* _db_manager;
	State _state = State::IDLE;

	void _state_machine_proc();

	StateAction _idle_proc();
	StateAction _decrypt_proc();
	StateAction _opened_proc();

	void _send_fail();
};


} /* namespace tasks */

#endif /* TASKS_DATABASETASK_H_ */
