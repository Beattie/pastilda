/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TASKS_OUTPUTMANAGETASK_H_
#define TASKS_OUTPUTMANAGETASK_H_

#include <new>
#include <lang/StringFixed.hpp>
#include <hw/kbd/Package.hpp>
#include <database/Entry.h>
#include <config/TasksConfig.h>

#include "common/Message.hpp"
#include "common/IKahnProcessTask.hpp"

namespace tasks {

namespace msg {

enum class OutputManagerCommand : uint8_t {
    PASTILDA_START,
	MESSAGE,
	PASSWORD,
	DB_ERROR_MESSAGE,
	CREDENTIALS,
	END,
};

union OutputManagerData
{
    using StringType = lang::StringFixed<config::OutputManagerTask::MAX_MESSAGE_STRING_SIZE>;

public:
    StringType str;
    db::Entry* db_entry;

    OutputManagerData() {
        new(&str) StringType();
    }

    ~OutputManagerData() {
        str.~StringType();
    }
};

using OutputManagerMessage = common::Message<OutputManagerCommand, OutputManagerData>;


} /* namespace msg */


class OutputManagerTask final :
	public common::IKahnProcessTask<config::OutputManagerTask::NAME,
	                                msg::OutputManagerMessage,
								    config::OutputManagerTask::FIFO_SIZE>
{
public:
    enum State {
        IDLE,
        PASSWORD,
        PASSWORD_FAIL,
        NAVIGATION,
        CLEANING,
        LOGIN,
        END,
    };

    enum StateAction {
        STAY,
        NEXT,
        FAIL,
        QUIT,
    };

    OutputManagerTask();
	virtual ~OutputManagerTask() = default;

	void Run();

private:
	class Model;
	class Printer;

    Model* _model;
    Printer* _printer;

    State _state = State::IDLE;

	void _state_machine_proc();

	StateAction _idle_proc();
	StateAction _password_proc();
	StateAction _password_fail_proc();
	StateAction _navigation_proc();
	StateAction _cleaning_proc();
	StateAction _login_proc();
	StateAction _end_proc();
};


} /* namespace tasks */

#endif /* TASKS_OUTPUTMANAGETASK_H_ */
