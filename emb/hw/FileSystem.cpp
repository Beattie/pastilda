/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FileSystem.h"

namespace hw {

FileSystem::FatFs FileSystem::_fs;

FileSystem::FileSystem()
{
	SdCard::init();
	f_mount(0, &_fs);
}

int FileSystem::msd_read(uint32_t block, uint8_t *buffer)
{
	BaseType_t interrupt_state;

	memset(buffer, 0, sdcard::BLOCK_SIZE);
	if (block >= msd_blocks()) {
		return (1);
	}

	else
	{
		if (rtos::Thread::IsSchedulerActive()) {
			interrupt_state = rtos::CriticalSection::EnterFromISR();
		}

		if (SdCard::read_block(buffer, block << 9) == SdCardResult::SD_OK)
		{
			if (SdCard::wait_read_operation() == SdCardResult::SD_OK)
			{
				while(SdCard::get_status() != hw::sdcard::SD_TRANSFER_OK);

				if (rtos::Thread::IsSchedulerActive()) {
					rtos::CriticalSection::ExitFromISR(interrupt_state);
				}
				return (0);
			}
		}

		if (rtos::Thread::IsSchedulerActive()) {
			rtos::CriticalSection::ExitFromISR(interrupt_state);
		}
		return (1);
	}
}

int FileSystem::msd_write(uint32_t block, const uint8_t *buffer)
{
	BaseType_t interrupt_state;

	if (block >= msd_blocks()) {
		return (1);
	}

	else
	{
		if (rtos::Thread::IsSchedulerActive()) {
			interrupt_state = rtos::CriticalSection::EnterFromISR();
		}

		if (SdCard::write_block((uint8_t*)buffer, block << 9) == SdCardResult::SD_OK)
		{
			if (SdCard::wait_write_operation() == SdCardResult::SD_OK)
			{
				while(SdCard::get_status() != hw::sdcard::SD_TRANSFER_OK);

				if (rtos::Thread::IsSchedulerActive()) {
					rtos::CriticalSection::ExitFromISR(interrupt_state);
				}
				return 0;
			}
		}

		if (rtos::Thread::IsSchedulerActive()) {
			rtos::CriticalSection::ExitFromISR(interrupt_state);
		}
		return (1);
	}
}

int FileSystem::msd_blocks(void)
{
	return (uint32_t)(SdCard::sdcard_info.CardCapacity / sdcard::BLOCK_SIZE);
}

auto FileSystem::open_file_to_read(File *file, const char *name) -> Result
{
	return (f_open(file, name, FA_OPEN_EXISTING | FA_READ));
}

auto FileSystem::open_file_to_write(File *file, const char *name) -> Result
{
	return (f_open(file, name, FA_CREATE_ALWAYS | FA_WRITE));
}

auto FileSystem::close_file(File *file) -> Result
{
	return (f_close(file));
}

auto FileSystem::read_next_file_chunk(File *file, void *buffer, uint32_t size) -> Result
{
	UINT nRead;
	return (f_read(file, buffer, size, &nRead));
}

auto FileSystem::write_next_file_chunk(File *file, void *buffer, uint32_t size) -> Result
{
	UINT nWritten;
	return (f_write(file, buffer, size, &nWritten));
}

uint32_t FileSystem::get_file_tell(File *file)
{
	return (f_tell(file));
}

}
