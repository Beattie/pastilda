/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "UsbDevice.h"
#include <libopencm3/stm32/otg_fs.h>

namespace hw {

namespace usb {

constexpr uint8_t UsbDevice::keyboard_report_descriptor[];
constexpr UsbDevice::type_hid_function  UsbDevice::keyboard_hid_function;
constexpr struct usb_device_descriptor UsbDevice::dev;
constexpr struct usb_endpoint_descriptor UsbDevice::hid_endpoint;
constexpr struct usb_endpoint_descriptor UsbDevice::msc_endpoint[];
constexpr struct usb_interface_descriptor UsbDevice::iface[];
constexpr struct usb_config_descriptor::usb_interface UsbDevice::ifaces[];
constexpr struct usb_config_descriptor UsbDevice::config_descr;
constexpr char UsbDevice::usb_strings[][30];

UsbDevice::UsbDevice()
{
	GPIO_CPP_Extension::GPIO_ext uf_p(config::USB_DEVICE_P);
	GPIO_CPP_Extension::GPIO_ext uf_m(config::USB_DEVICE_M);

	uf_p.mode_setup(GPIO_CPP_Extension::Mode::ALTERNATE_FUNCTION,
			       GPIO_CPP_Extension::PullMode::NO_PULL);

	uf_m.mode_setup(GPIO_CPP_Extension::Mode::ALTERNATE_FUNCTION,
			        GPIO_CPP_Extension::PullMode::NO_PULL);

	uf_p.set_af(config::USB_DEVICE_AF);
	uf_m.set_af(config::USB_DEVICE_AF);

	device = usbd_init(&otgfs_usb_driver, &dev, &config_descr,
			           (const char**)usb_strings, 3, _buffer, sizeof(_buffer));
}

void UsbDevice::msd_init()
{
	hw::FileSystem::Instance();

    usb_msc_init(device,
    			 Endpoint::E_MASS_STORAGE_IN, 64,
				 Endpoint::E_MASS_STORAGE_OUT, 64,
                 "ThirdPin", "Pastilda", "0.01",
				 hw::FileSystem::msd_blocks(),
				 hw::FileSystem::msd_read,
				 hw::FileSystem::msd_write);
}

void UsbDevice::enable_interrupt()
{
    nvic_set_priority(hw::config::USB_DEVICE_IRQ, hw::config::USB_DEVICE_IRQ_PRIORITY);
    nvic_enable_irq(hw::config::USB_DEVICE_IRQ);
}

void UsbDevice::disable_interrupt()
{
	nvic_disable_irq(hw::config::USB_DEVICE_IRQ);
}

//TODO: [USB] move is_host_suspend && remote_wakeup to driver, remove magic numbers
bool UsbDevice::is_host_suspend(void)
{
	return( (OTG_FS_DSTS & 0x01)&1 ); //bit SUSPSTS: Suspend status
}

void UsbDevice::remote_wakeup(bool on)
{
	if(on)
		OTG_FS_DCTL |= 0x00000001; //bit RWUSIG: Remote wakeup signaling
	else
		OTG_FS_DCTL &= ~0x00000001;
}


} /* namespace usb */

} /* namespace hw */
