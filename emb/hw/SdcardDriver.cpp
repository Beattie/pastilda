/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SdcardDriver.h"

#define NULL 0

namespace hw {

namespace sdcard {

static uint32_t CardType = sdcard::SDIO_STD_CAPACITY_SD_CARD_V1_1;
static uint32_t CSD_Tab[4] = {0};
static uint32_t CID_Tab[4] = {0};
static uint32_t RCA = 0;
static uint8_t SDSTATUS_Tab[16];
uint32_t StopCondition = 0;
sdcard::Result TransferError = sdcard::SD_OK;
uint32_t TransferEnd = 0;
uint32_t DMAEndOfTransfer = 0;

SDIO_config _config;

SDIO_config SdcardDriver::_config;
SDIO_command SdcardDriver::_command;
SDIO_data_config SdcardDriver::_data_config;
GPIO_CPP_Extension::GPIO_ext SdcardDriver::_sd_det;
sdcard::CardInfo SdcardDriver::sdcard_info;
bool SdcardDriver::is_initialized = false;

sdcard::Result SdcardDriver::init()
{
	_sdio_config();

	sdcard::Result result = sdcard::SD_OK;

//	if (_get_state() == sdcard::SD_NOT_PRESENT)
//		return sdcard::Result::SD_ERROR;

	sdio_deinit();

	_config.bypass = false;
	_config.bus_wide = WIDE_BUS_1;
	_config.clock_divider = sdcard::INIT_DIVIDER;
	_config.polarity = SDIO_CK_RISING_EDGE;
	_config.flow_control = false;
	_config.power_save = false;
	sdio_config(&_config);

  	result = _power_on();

  	if (result != sdcard::SD_OK)
  		return result;

  	result = _cards_init();

  	if (result != sdcard::SD_OK)
  		return result;

  	_config.bypass = false;
  	_config.bus_wide = WIDE_BUS_1;
  	_config.clock_divider = sdcard::TRANSFER_DIVIDER;
  	_config.polarity = SDIO_CK_RISING_EDGE;
  	_config.flow_control = false;
  	_config.power_save = false;
	sdio_config(&_config);

   	result = _get_card_info(&sdcard_info);

  	if (result == sdcard::SD_OK)
    	result = _select_deselect((uint32_t)(sdcard_info.RCA << 16));

  	if (result == sdcard::SD_OK)
    	result = _enable_wide_bus_operation(WIDE_BUS_4);

  	if (result == sdcard::SD_OK)
  		is_initialized = true;
	return (result);
}

sdcard::Result SdcardDriver::read_block(uint8_t *buffer, uint64_t address)
{
	sdcard::Result result = sdcard::SD_OK;

  	TransferError = sdcard::SD_OK;
  	TransferEnd = 0;
  	StopCondition = 0;

  	SDIO_DCTRL = 0x0;

    sdio_enable_interrupt(SDIO_DCRCFAIL | SDIO_DTIMEOUT | SDIO_DATAEND | SDIO_RXOVERR | SDIO_STBITERR);
    sdio_enable_dma();
    _dma_rx_config((uint32_t *)buffer, sdcard::BLOCK_SIZE);

  	if (CardType == sdcard::SDIO_HIGH_CAPACITY_SD_CARD)
  	{
  		address /= 512;
  	}

  	_command.index = sdcard::SD_CMD_SET_BLOCKLEN;
  	_command.arg = sdcard::BLOCK_SIZE;
  	_command.response = RESPONSE_SHORT;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_SET_BLOCKLEN);

  	if (result != sdcard::SD_OK)
  		return result;

  	_data_config.direction = CARD_TO_CONTROLLER;
  	_data_config.period = sdcard::SD_DATATIMEOUT;
  	_data_config.size = BLOCK_LENGTH_512_B;
  	_data_config.length = sdcard::BLOCK_SIZE;
  	_data_config.mode = BLOCK_DATA_TRANSFER;
  	_data_config.enable = true;
	sdio_data_config(&_data_config);

	_command.index = sdcard::SD_CMD_READ_SINGLE_BLOCK;
	_command.arg = (uint32_t)address;
	_command.response = RESPONSE_SHORT;
	_command.wait = WAIT_NO;
	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_READ_SINGLE_BLOCK);

  	if (result != sdcard::SD_OK)
  		return result;

  	return(result);
}

sdcard::Result SdcardDriver::write_block(uint8_t *buffer, uint64_t address)
{
	sdcard::Result result = sdcard::SD_OK;

	TransferError = sdcard::SD_OK;
	TransferEnd = 0;
	StopCondition = 0;

    SDIO_DCTRL = 0x0;

    sdio_enable_interrupt(SDIO_DCRCFAIL | SDIO_DTIMEOUT | SDIO_DATAEND | SDIO_RXOVERR | SDIO_STBITERR);
    sdio_enable_dma();
    _dma_tx_config((uint32_t *)buffer, sdcard::BLOCK_SIZE);


  	if (CardType == sdcard::SDIO_HIGH_CAPACITY_SD_CARD)
  	{
    	address /= 512;
  	}

  	_command.index = sdcard::SD_CMD_SET_BLOCKLEN;
  	_command.arg = sdcard::BLOCK_SIZE;
  	_command.response = RESPONSE_SHORT;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_SET_BLOCKLEN);

  	if (result != sdcard::SD_OK)
    	return(result);

  	_command.index = sdcard::SD_CMD_WRITE_SINGLE_BLOCK;
  	_command.arg = (uint32_t)address;
  	_command.response = RESPONSE_SHORT;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_WRITE_SINGLE_BLOCK);

  	if (result != sdcard::SD_OK)
    	return(result);

  	_data_config.direction = CONTROLLER_TO_CARD;
  	_data_config.period = sdcard::SD_DATATIMEOUT;
  	_data_config.size = BLOCK_LENGTH_512_B;
  	_data_config.length = sdcard::BLOCK_SIZE;
  	_data_config.mode = BLOCK_DATA_TRANSFER;
  	_data_config.enable = true;
	sdio_data_config(&_data_config);

  	return(result);
}

sdcard::Result SdcardDriver::read_blocks(uint8_t *buffer, uint64_t address, uint32_t blockcount)
{
	sdcard::Result result = sdcard::SD_OK;
	TransferError = sdcard::SD_OK;
	TransferEnd = 0;
	StopCondition = 1;

	SDIO_DCTRL = 0x0;

    sdio_enable_interrupt(SDIO_DCRCFAIL | SDIO_DTIMEOUT | SDIO_DATAEND | SDIO_RXOVERR | SDIO_STBITERR);
    sdio_enable_dma();
    _dma_rx_config((uint32_t *)buffer, (sdcard::BLOCK_SIZE * blockcount));

  	if (CardType == sdcard::SDIO_HIGH_CAPACITY_SD_CARD)
  	{
  		address /= 512;
  	}

  	_command.index = sdcard::SD_CMD_SET_BLOCKLEN;
  	_command.arg = sdcard::BLOCK_SIZE;
  	_command.response = RESPONSE_SHORT;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_SET_BLOCKLEN);

  	if (result != sdcard::SD_OK)
  		return result;

  	_data_config.direction = CARD_TO_CONTROLLER;
  	_data_config.period = sdcard::SD_DATATIMEOUT;
  	_data_config.size = BLOCK_LENGTH_512_B;
  	_data_config.length = (sdcard::BLOCK_SIZE * blockcount);
  	_data_config.mode = BLOCK_DATA_TRANSFER;
  	_data_config.enable = true;
	sdio_data_config(&_data_config);

	_command.index = sdcard::SD_CMD_READ_MULT_BLOCK;
	_command.arg = (uint32_t)address;
	_command.response = RESPONSE_SHORT;
	_command.wait = WAIT_NO;
	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_READ_MULT_BLOCK);

  	if (result != sdcard::SD_OK)
  		return result;

  	return(result);
}

sdcard::Result SdcardDriver::write_blocks(uint8_t *buffer, uint64_t address, uint32_t blockcount)
{
	sdcard::Result result = sdcard::SD_OK;

	TransferError = sdcard::SD_OK;
	TransferEnd = 0;
	StopCondition = 1;

    SDIO_DCTRL = 0x0;

    sdio_enable_interrupt(SDIO_DCRCFAIL | SDIO_DTIMEOUT | SDIO_DATAEND | SDIO_RXOVERR | SDIO_STBITERR);
    sdio_enable_dma();
    _dma_tx_config((uint32_t *)buffer, (sdcard::BLOCK_SIZE * blockcount));

  	if (CardType == sdcard::SDIO_HIGH_CAPACITY_SD_CARD)
  	{
  		address /= 512;
  	}

  	_command.index = sdcard::SD_CMD_SET_BLOCKLEN;
  	_command.arg = sdcard::BLOCK_SIZE;
  	_command.response = RESPONSE_SHORT;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_SET_BLOCKLEN);

  	if (result != sdcard::SD_OK)
    	return(result);

  	_command.index = sdcard::SD_CMD_APP_CMD;
  	_command.arg = (uint32_t)(uint32_t) (RCA << 16);
  	_command.response = RESPONSE_SHORT;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);

  	if (result != sdcard::SD_OK)
    	return(result);

  	_command.index = sdcard::SD_CMD_SET_BLOCK_COUNT;
  	_command.arg = (uint32_t)blockcount;
  	_command.response = RESPONSE_SHORT;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_SET_BLOCK_COUNT);

  	if (result != sdcard::SD_OK)
    	return(result);

  	_command.index = sdcard::SD_CMD_WRITE_MULT_BLOCK;
  	_command.arg = (uint32_t)address;
  	_command.response = RESPONSE_SHORT;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_WRITE_MULT_BLOCK);

  	if (result != sdcard::SD_OK)
  		return(result);

  	_data_config.direction = CONTROLLER_TO_CARD;
  	_data_config.period = sdcard::SD_DATATIMEOUT;
  	_data_config.size = BLOCK_LENGTH_512_B;
  	_data_config.length = (sdcard::BLOCK_SIZE * blockcount);
  	_data_config.mode = BLOCK_DATA_TRANSFER;
  	_data_config.enable = true;
	sdio_data_config(&_data_config);

  	return(result);
}

sdcard::Result SdcardDriver::wait_read_operation(void)
{
	sdcard::Result result = sdcard::SD_OK;
	uint32_t timeout = sdcard::SD_DATATIMEOUT;

	while ((DMAEndOfTransfer == 0x00) && (TransferEnd == 0) && (TransferError == sdcard::SD_OK) && (timeout > 0))
	{
		timeout--;
	}

	DMAEndOfTransfer = 0x00;
	timeout = sdcard::SD_DATATIMEOUT;

	while(((SDIO_STA & SDIO_RXACT)) && (timeout > 0))
	{
		timeout--;
	}

	if (StopCondition == 1)
	{
		result = _stop_transfer();
		StopCondition = 0;
	}

	if ((timeout == 0) && (result == sdcard::SD_OK))
	{
		result = sdcard::SD_DATA_TIMEOUT;
	}

	sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);

	if (TransferError != sdcard::SD_OK)
	{
		return(TransferError);
	}
	else
	{
		return(result);
	}
}

sdcard::Result SdcardDriver::wait_write_operation(void)
{
	sdcard::Result result = sdcard::SD_OK;
	uint32_t timeout = sdcard::SD_DATATIMEOUT;

	while ((DMAEndOfTransfer == 0x00) && (TransferEnd == 0) && (TransferError == sdcard::SD_OK) && (timeout!=0))
	{
			timeout--;
	}

	DMAEndOfTransfer = 0x00;
	timeout = sdcard::SD_DATATIMEOUT;

	while(((SDIO_STA & SDIO_TXACT)) && (timeout > 0))
	{
		timeout--;
	}

	if (StopCondition == 1)
	{
		result = _stop_transfer();
		StopCondition = 0;
	}

	if ((timeout == 0) && (result == sdcard::SD_OK))
	{
		result = sdcard::SD_DATA_TIMEOUT;
	}

	sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);

	if (TransferError != sdcard::SD_OK)
	{
		return(TransferError);
	}
	else
	{
		return(result);
	}
}

sdcard::Result SdcardDriver::erase(uint64_t startaddr, uint64_t endaddr)
{
	sdcard::Result result = sdcard::SD_OK;
  	uint32_t delay = 0;
  	uint32_t maxdelay = 0;
  	uint8_t cardstate = 0;

  	if (((CSD_Tab[1] >> 20) & sdcard::SD_CCCC_ERASE) == 0)
  	{
    	result = sdcard::SD_REQUEST_NOT_APPLICABLE;
    	return(result);
  	}

  	maxdelay = 120000 / (sdcard::TRANSFER_DIVIDER + 2);

  	if (sdio_get_card_status_1() & sdcard::SD_CARD_LOCKED)
  	{
    	result = sdcard::SD_LOCK_UNLOCK_FAILED;
    	return(result);
  	}

  	if (CardType == sdcard::SDIO_HIGH_CAPACITY_SD_CARD)
  	{
    	startaddr /= 512;
    	endaddr /= 512;
  	}

  	if ((sdcard::SDIO_STD_CAPACITY_SD_CARD_V1_1 == CardType) ||
  		(sdcard::SDIO_STD_CAPACITY_SD_CARD_V2_0 == CardType) ||
		(sdcard::SDIO_HIGH_CAPACITY_SD_CARD == CardType))
  	{
  		_command.index = sdcard::SD_CMD_SD_ERASE_GRP_START;
  		_command.arg = startaddr;
  		_command.response = RESPONSE_SHORT;
  		_command.wait = WAIT_NO;
  		_command.enable_CPSM = true;
  	  	sdio_send_command(&_command);

  		result = _cmd_R1_error(sdcard::SD_CMD_SD_ERASE_GRP_START);

   		if (result != sdcard::SD_OK)
      		return(result);

   		_command.index = sdcard::SD_CMD_SD_ERASE_GRP_END;
   		_command.arg = endaddr;
   		_command.response = RESPONSE_SHORT;
   		_command.wait = WAIT_NO;
   		_command.enable_CPSM = true;
  	  	sdio_send_command(&_command);

      	result = _cmd_R1_error(sdcard::SD_CMD_SD_ERASE_GRP_END);

   		if (result != sdcard::SD_OK)
      		return(result);
  	}

  	_command.index = sdcard::SD_CMD_ERASE;
  	_command.arg = 0;
  	_command.response = RESPONSE_SHORT;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_ERASE);

  	if (result != sdcard::SD_OK)
    	return(result);

  	for (delay = 0; delay < maxdelay; delay++) { }

  	result = _is_busy(&cardstate);
  	delay = sdcard::SD_DATATIMEOUT;

  	while ((delay > 0) && (result == sdcard::SD_OK) && ((sdcard::SD_CARD_PROGRAMMING == cardstate) || (sdcard::SD_CARD_RECEIVING == cardstate)))
  	{
    	result = _is_busy(&cardstate);
    	delay--;
  	}

  	return(result);
}

sdcard::TransferState SdcardDriver::get_status(void)
{
	sdcard::State cardstate =  sdcard::SD_CARD_TRANSFER;
  	cardstate = _get_state();

  	if (cardstate == sdcard::SD_CARD_TRANSFER)
  	{
    	return(sdcard::SD_TRANSFER_OK);
  	}
  	else if(cardstate == sdcard::SD_CARD_ERROR)
  	{
    	return (sdcard::SD_TRANSFER_ERROR);
  	}
  	else
  	{
    	return(sdcard::SD_TRANSFER_BUSY);
    }
}

uint8_t SdcardDriver::detect(void)
{
	uint8_t status = sdcard::SD_PRESENT;

	if (_sd_det.get() != 0)
	{
		status = sdcard::SD_NOT_PRESENT;
	}

	return status;
}

sdcard::Result SdcardDriver::sdio_irq_proc(void)
{
	  if (sdio_get_flag_status(SDIO_DATAEND))
	  {
	    TransferError = sdcard::SD_OK;
	    sdio_clear_flag_status(SDIO_DATAEND);
	    TransferEnd = 1;
	  }
	  else if (sdio_get_flag_status(SDIO_DCRCFAIL))
	  {
		  sdio_clear_flag_status(SDIO_DCRCFAIL);
	    TransferError = sdcard::SD_DATA_CRC_FAIL;
	  }
	  else if (sdio_get_flag_status(SDIO_DTIMEOUT))
	  {
		  sdio_clear_flag_status(SDIO_DTIMEOUT);
	    TransferError = sdcard::SD_DATA_TIMEOUT;
	  }
	  else if (sdio_get_flag_status(SDIO_RXOVERR))
	  {
		  sdio_clear_flag_status(SDIO_RXOVERR);
	    TransferError = sdcard::SD_RX_OVERRUN;
	  }
	  else if (sdio_get_flag_status(SDIO_TXUNDERR))
	  {
		  sdio_clear_flag_status(SDIO_TXUNDERR);
	    TransferError = sdcard::SD_TX_UNDERRUN;
	  }
	  else if (sdio_get_flag_status(SDIO_STBITERR))
	  {
		  sdio_clear_flag_status(SDIO_STBITERR);
	    TransferError = sdcard::SD_START_BIT_ERR;
	  }

	  sdio_disable_interrupt(SDIO_DCRCFAIL | SDIO_DTIMEOUT | SDIO_DATAEND |
			  	  	  	  	 SDIO_TXFIFOHE | SDIO_RXFIFOHF | SDIO_TXUNDERR |
							 SDIO_RXOVERR | SDIO_STBITERR);
	  return(TransferError);
}

void SdcardDriver::dma_irq_proc(void)
{
  if(dma_get_interrupt_flag(config::SDIO_DMA, config::SDIO_DMA_STREAM, DMA_HISR_TCIF4))
  {
    DMAEndOfTransfer = 0x01;
    dma_clear_interrupt_flags(config::SDIO_DMA, config::SDIO_DMA_STREAM, (DMA_HISR_TCIF4 | DMA_HISR_FEIF4));
  }
}

sdcard::Result SdcardDriver::_power_on(void)
{
	sdcard::Result result = sdcard::SD_OK;
  	uint32_t response = 0, count = 0, validvoltage = 0;
 	uint32_t SDType = sdcard::SD_STD_CAPACITY;

  	sdio_enable();
  	sdio_ck_enable();

  	_command.index = sdcard::SD_CMD_GO_IDLE_STATE;
  	_command.arg = 0;
  	_command.response = RESPONSE_NO;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_error();

  	if (result != sdcard::SD_OK)
  		return result;

  	_command.index = sdcard::SDIO_SEND_IF_COND;
  	_command.arg = sdcard::SD_CHECK_PATTERN;
  	_command.response = RESPONSE_SHORT;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R7_error();

  	if (result == sdcard::SD_OK)
  	{
    	CardType = sdcard::SDIO_STD_CAPACITY_SD_CARD_V2_0;
    	SDType = sdcard::SD_HIGH_CAPACITY;
  	}

  	else
  	{
  		_command.index = sdcard::SD_CMD_APP_CMD;
  		_command.arg = 0;
  		_command.response = RESPONSE_SHORT;
  		_command.wait = WAIT_NO;
  		_command.enable_CPSM = true;
  	  	sdio_send_command(&_command);

    	result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);
  	}

  	_command.index = sdcard::SD_CMD_APP_CMD;
  	_command.arg = 0;
  	_command.response = RESPONSE_SHORT;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);

  	if (result == sdcard::SD_OK)
  	{
    	while ((!validvoltage) && (count < sdcard::SD_MAX_VOLT_TRIAL))
    	{
    		_command.index = sdcard::SD_CMD_APP_CMD;
    		_command.arg = 0;
    		_command.response = RESPONSE_SHORT;
    		_command.wait = WAIT_NO;
    		_command.enable_CPSM = true;
    	  	sdio_send_command(&_command);

      		result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);

  			if (result != sdcard::SD_OK)
  				return result;

  			_command.index = sdcard::SD_CMD_SD_APP_OP_COND;
  			_command.arg = (sdcard::SD_VOLTAGE_WINDOW_SD | SDType);
  			_command.response = RESPONSE_SHORT;
  			_command.wait = WAIT_NO;
  			_command.enable_CPSM = true;
  		  	sdio_send_command(&_command);

      		result = _cmd_R3_error();

  			if (result != sdcard::SD_OK)
  				return result;

      		response = sdio_get_card_status_1();
      		validvoltage = (((response >> 31) == 1) ? 1 : 0);
      		count++;
    	}

    	if (count >= sdcard::SD_MAX_VOLT_TRIAL)
    	{
      		result = sdcard::SD_INVALID_VOLTRANGE;
      		return(result);
    	}

    	if (response &= sdcard::SD_HIGH_CAPACITY)
    	{
      		CardType = sdcard::SDIO_HIGH_CAPACITY_SD_CARD;
    	}
	}

	return(result);
}

sdcard::Result SdcardDriver::_power_off(void)
{
  	sdio_disable();
  	return sdcard::SD_OK;
}

sdcard::Result SdcardDriver::_cards_init(void)
{
	sdcard::Result result = sdcard::SD_OK;
  	uint16_t rca = 0x01;

  	if (sdcard::SDIO_SECURE_DIGITAL_IO_CARD != CardType)
  	{
  		_command.index = sdcard::SD_CMD_ALL_SEND_CID;
  		_command.arg = 0;
  		_command.response = RESPONSE_LONG;
  		_command.wait = WAIT_NO;
  		_command.enable_CPSM = true;
  		sdio_send_command(&_command);

    	result = _cmd_R2_error();

  		if (result != sdcard::SD_OK)
  			return result;

    	CID_Tab[0] = sdio_get_card_status_1();
    	CID_Tab[1] = sdio_get_card_status_2();
    	CID_Tab[2] = sdio_get_card_status_3();
    	CID_Tab[3] = sdio_get_card_status_4();
  	}

  	if ((sdcard::SDIO_STD_CAPACITY_SD_CARD_V1_1 == CardType) ||
  		(sdcard::SDIO_STD_CAPACITY_SD_CARD_V2_0 == CardType) ||
  		(sdcard::SDIO_SECURE_DIGITAL_IO_COMBO_CARD == CardType) ||
  		(sdcard::SDIO_HIGH_CAPACITY_SD_CARD == CardType))
  	{
  		_command.index = sdcard::SD_CMD_SET_REL_ADDR;
  		_command.arg = 0;
  		_command.response = RESPONSE_SHORT;
  		_command.wait = WAIT_NO;
  		_command.enable_CPSM = true;
  		sdio_send_command(&_command);

    	result = _cmd_R6_error(sdcard::SD_CMD_SET_REL_ADDR, &rca);

  		if (result != sdcard::SD_OK)
  			return result;
  	}

  	if (sdcard::SDIO_SECURE_DIGITAL_IO_CARD != CardType)
  	{
    	RCA = rca;

    	_command.index = sdcard::SD_CMD_SEND_CSD;
    	_command.arg = (uint32_t)(rca << 16);
    	_command.response = RESPONSE_LONG;
    	_command.wait = WAIT_NO;
  		_command.enable_CPSM = true;
  		sdio_send_command(&_command);

    	result = _cmd_R2_error();

  		if (result != sdcard::SD_OK)
  			return result;

    	CSD_Tab[0] = sdio_get_card_status_1();
    	CSD_Tab[1] = sdio_get_card_status_2();
    	CSD_Tab[2] = sdio_get_card_status_3();
    	CSD_Tab[3] = sdio_get_card_status_4();
  	}

  	result = sdcard::SD_OK;
  	return(result);
}

sdcard::Result SdcardDriver::_get_card_info(sdcard::CardInfo *cardinfo)
{
	sdcard::Result result = sdcard::SD_OK;
  	uint8_t tmp = 0;

  	cardinfo->CardType = (uint8_t)CardType;
  	cardinfo->RCA = (uint16_t)RCA;

  	/*!< Byte 0 */
  	tmp = (uint8_t)((CSD_Tab[0] & 0xFF000000) >> 24);
  	cardinfo->SD_csd.CSDStruct = (tmp & 0xC0) >> 6;
  	cardinfo->SD_csd.SysSpecVersion = (tmp & 0x3C) >> 2;
  	cardinfo->SD_csd.Reserved1 = tmp & 0x03;

  	/*!< Byte 1 */
  	tmp = (uint8_t)((CSD_Tab[0] & 0x00FF0000) >> 16);
  	cardinfo->SD_csd.TAAC = tmp;

  	/*!< Byte 2 */
  	tmp = (uint8_t)((CSD_Tab[0] & 0x0000FF00) >> 8);
  	cardinfo->SD_csd.NSAC = tmp;

  	/*!< Byte 3 */
  	tmp = (uint8_t)(CSD_Tab[0] & 0x000000FF);
  	cardinfo->SD_csd.MaxBusClkFrec = tmp;

  	/*!< Byte 4 */
  	tmp = (uint8_t)((CSD_Tab[1] & 0xFF000000) >> 24);
  	cardinfo->SD_csd.CardComdClasses = tmp << 4;

  	/*!< Byte 5 */
  	tmp = (uint8_t)((CSD_Tab[1] & 0x00FF0000) >> 16);
  	cardinfo->SD_csd.CardComdClasses |= (tmp & 0xF0) >> 4;
  	cardinfo->SD_csd.RdBlockLen = tmp & 0x0F;

  	/*!< Byte 6 */
  	tmp = (uint8_t)((CSD_Tab[1] & 0x0000FF00) >> 8);
  	cardinfo->SD_csd.PartBlockRead = (tmp & 0x80) >> 7;
  	cardinfo->SD_csd.WrBlockMisalign = (tmp & 0x40) >> 6;
  	cardinfo->SD_csd.RdBlockMisalign = (tmp & 0x20) >> 5;
  	cardinfo->SD_csd.DSRImpl = (tmp & 0x10) >> 4;
  	cardinfo->SD_csd.Reserved2 = 0; /*!< Reserved */

  	if ((CardType == sdcard::SDIO_STD_CAPACITY_SD_CARD_V1_1) ||
  		(CardType == sdcard::SDIO_STD_CAPACITY_SD_CARD_V2_0))
  	{
    	cardinfo->SD_csd.DeviceSize = (tmp & 0x03) << 10;

    	/*!< Byte 7 */
    	tmp = (uint8_t)(CSD_Tab[1] & 0x000000FF);
    	cardinfo->SD_csd.DeviceSize |= (tmp) << 2;

    	/*!< Byte 8 */
    	tmp = (uint8_t)((CSD_Tab[2] & 0xFF000000) >> 24);
    	cardinfo->SD_csd.DeviceSize |= (tmp & 0xC0) >> 6;

    	cardinfo->SD_csd.MaxRdCurrentVDDMin = (tmp & 0x38) >> 3;
    	cardinfo->SD_csd.MaxRdCurrentVDDMax = (tmp & 0x07);

    	/*!< Byte 9 */
    	tmp = (uint8_t)((CSD_Tab[2] & 0x00FF0000) >> 16);
    	cardinfo->SD_csd.MaxWrCurrentVDDMin = (tmp & 0xE0) >> 5;
    	cardinfo->SD_csd.MaxWrCurrentVDDMax = (tmp & 0x1C) >> 2;
    	cardinfo->SD_csd.DeviceSizeMul = (tmp & 0x03) << 1;

    	/*!< Byte 10 */
    	tmp = (uint8_t)((CSD_Tab[2] & 0x0000FF00) >> 8);
    	cardinfo->SD_csd.DeviceSizeMul |= (tmp & 0x80) >> 7;

    	cardinfo->CardCapacity = (cardinfo->SD_csd.DeviceSize + 1) ;
    	cardinfo->CardCapacity *= (1 << (cardinfo->SD_csd.DeviceSizeMul + 2));
    	cardinfo->CardBlockSize = 1 << (cardinfo->SD_csd.RdBlockLen);
    	cardinfo->CardCapacity *= cardinfo->CardBlockSize;
  	}
  	else if (CardType == sdcard::SDIO_HIGH_CAPACITY_SD_CARD)
  	{
    	/*!< Byte 7 */
    	tmp = (uint8_t)(CSD_Tab[1] & 0x000000FF);
    	cardinfo->SD_csd.DeviceSize = (tmp & 0x3F) << 16;

    	/*!< Byte 8 */
    	tmp = (uint8_t)((CSD_Tab[2] & 0xFF000000) >> 24);

    	cardinfo->SD_csd.DeviceSize |= (tmp << 8);

    	/*!< Byte 9 */
    	tmp = (uint8_t)((CSD_Tab[2] & 0x00FF0000) >> 16);

    	cardinfo->SD_csd.DeviceSize |= (tmp);

    	/*!< Byte 10 */
    	tmp = (uint8_t)((CSD_Tab[2] & 0x0000FF00) >> 8);

    	cardinfo->CardCapacity = ((uint64_t)cardinfo->SD_csd.DeviceSize + 1) * 512 * 1024;
    	cardinfo->CardBlockSize = 512;
  	}


  	cardinfo->SD_csd.EraseGrSize = (tmp & 0x40) >> 6;
  	cardinfo->SD_csd.EraseGrMul = (tmp & 0x3F) << 1;

  	/*!< Byte 11 */
  	tmp = (uint8_t)(CSD_Tab[2] & 0x000000FF);
  	cardinfo->SD_csd.EraseGrMul |= (tmp & 0x80) >> 7;
  	cardinfo->SD_csd.WrProtectGrSize = (tmp & 0x7F);

  	/*!< Byte 12 */
  	tmp = (uint8_t)((CSD_Tab[3] & 0xFF000000) >> 24);
  	cardinfo->SD_csd.WrProtectGrEnable = (tmp & 0x80) >> 7;
  	cardinfo->SD_csd.ManDeflECC = (tmp & 0x60) >> 5;
  	cardinfo->SD_csd.WrSpeedFact = (tmp & 0x1C) >> 2;
  	cardinfo->SD_csd.MaxWrBlockLen = (tmp & 0x03) << 2;

  	/*!< Byte 13 */
  	tmp = (uint8_t)((CSD_Tab[3] & 0x00FF0000) >> 16);
  	cardinfo->SD_csd.MaxWrBlockLen |= (tmp & 0xC0) >> 6;
  	cardinfo->SD_csd.WriteBlockPaPartial = (tmp & 0x20) >> 5;
  	cardinfo->SD_csd.Reserved3 = 0;
  	cardinfo->SD_csd.ContentProtectAppli = (tmp & 0x01);

  	/*!< Byte 14 */
  	tmp = (uint8_t)((CSD_Tab[3] & 0x0000FF00) >> 8);
  	cardinfo->SD_csd.FileFormatGrouop = (tmp & 0x80) >> 7;
  	cardinfo->SD_csd.CopyFlag = (tmp & 0x40) >> 6;
  	cardinfo->SD_csd.PermWrProtect = (tmp & 0x20) >> 5;
  	cardinfo->SD_csd.TempWrProtect = (tmp & 0x10) >> 4;
  	cardinfo->SD_csd.FileFormat = (tmp & 0x0C) >> 2;
  	cardinfo->SD_csd.ECC = (tmp & 0x03);

  	/*!< Byte 15 */
  	tmp = (uint8_t)(CSD_Tab[3] & 0x000000FF);
  	cardinfo->SD_csd.CSD_CRC = (tmp & 0xFE) >> 1;
  	cardinfo->SD_csd.Reserved4 = 1;

  	/*!< Byte 0 */
  	tmp = (uint8_t)((CID_Tab[0] & 0xFF000000) >> 24);
  	cardinfo->SD_cid.ManufacturerID = tmp;

  	/*!< Byte 1 */
  	tmp = (uint8_t)((CID_Tab[0] & 0x00FF0000) >> 16);
  	cardinfo->SD_cid.OEM_AppliID = tmp << 8;

  	/*!< Byte 2 */
  	tmp = (uint8_t)((CID_Tab[0] & 0x000000FF00) >> 8);
  	cardinfo->SD_cid.OEM_AppliID |= tmp;

  	/*!< Byte 3 */
  	tmp = (uint8_t)(CID_Tab[0] & 0x000000FF);
  	cardinfo->SD_cid.ProdName1 = tmp << 24;

  	/*!< Byte 4 */
  	tmp = (uint8_t)((CID_Tab[1] & 0xFF000000) >> 24);
  	cardinfo->SD_cid.ProdName1 |= tmp << 16;

  	/*!< Byte 5 */
  	tmp = (uint8_t)((CID_Tab[1] & 0x00FF0000) >> 16);
  	cardinfo->SD_cid.ProdName1 |= tmp << 8;

  	/*!< Byte 6 */
  	tmp = (uint8_t)((CID_Tab[1] & 0x0000FF00) >> 8);
  	cardinfo->SD_cid.ProdName1 |= tmp;

  	/*!< Byte 7 */
  	tmp = (uint8_t)(CID_Tab[1] & 0x000000FF);
  	cardinfo->SD_cid.ProdName2 = tmp;

  	/*!< Byte 8 */
  	tmp = (uint8_t)((CID_Tab[2] & 0xFF000000) >> 24);
  	cardinfo->SD_cid.ProdRev = tmp;

  	/*!< Byte 9 */
  	tmp = (uint8_t)((CID_Tab[2] & 0x00FF0000) >> 16);
  	cardinfo->SD_cid.ProdSN = tmp << 24;

  	/*!< Byte 10 */
  	tmp = (uint8_t)((CID_Tab[2] & 0x0000FF00) >> 8);
  	cardinfo->SD_cid.ProdSN |= tmp << 16;

  	/*!< Byte 11 */
  	tmp = (uint8_t)(CID_Tab[2] & 0x000000FF);
  	cardinfo->SD_cid.ProdSN |= tmp << 8;

  	/*!< Byte 12 */
  	tmp = (uint8_t)((CID_Tab[3] & 0xFF000000) >> 24);
  	cardinfo->SD_cid.ProdSN |= tmp;

  	/*!< Byte 13 */
  	tmp = (uint8_t)((CID_Tab[3] & 0x00FF0000) >> 16);
  	cardinfo->SD_cid.Reserved1 |= (tmp & 0xF0) >> 4;
  	cardinfo->SD_cid.ManufactDate = (tmp & 0x0F) << 8;

  	/*!< Byte 14 */
  	tmp = (uint8_t)((CID_Tab[3] & 0x0000FF00) >> 8);
  	cardinfo->SD_cid.ManufactDate |= tmp;

  	/*!< Byte 15 */
  	tmp = (uint8_t)(CID_Tab[3] & 0x000000FF);
  	cardinfo->SD_cid.CID_CRC = (tmp & 0xFE) >> 1;
  	cardinfo->SD_cid.Reserved2 = 1;

  return(result);
}

sdcard::Result SdcardDriver::_select_deselect(uint64_t addr)
{
	sdcard::Result result;

	_command.index = sdcard::SD_CMD_SEL_DESEL_CARD;
	_command.arg = addr;
	_command.response = RESPONSE_SHORT;
	_command.wait = WAIT_NO;
	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_SEL_DESEL_CARD);
  	return(result);
}

sdcard::Result SdcardDriver::_enable_wide_bus_operation(SDIO_WideBusMode mode)
{
	sdcard::Result result = sdcard::SD_OK;

  	if (sdcard::SDIO_MULTIMEDIA_CARD == CardType)
  	{
    	result = sdcard::SD_UNSUPPORTED_FEATURE;
    	return(result);
  	}

  	else if ((sdcard::SDIO_STD_CAPACITY_SD_CARD_V1_1 == CardType) ||
  			 (sdcard::SDIO_STD_CAPACITY_SD_CARD_V2_0 == CardType) ||
			 (sdcard::SDIO_HIGH_CAPACITY_SD_CARD == CardType))
  	{
  		switch(mode)
  		{
  			case WIDE_BUS_8:
  				result = sdcard::SD_UNSUPPORTED_FEATURE;
      			return(result);

      		case WIDE_BUS_4:
      			result = _wide_bus(true);
      			if (result == sdcard::SD_OK)
      			{
      				_config.bypass = false;
      				_config.bus_wide = WIDE_BUS_4;
      				_config.clock_divider = sdcard::TRANSFER_DIVIDER;
      				_config.polarity = SDIO_CK_RISING_EDGE;
      				_config.flow_control = false;
      				_config.power_save = false;
      				sdio_config(&_config);
      			}
        		break;

        	case WIDE_BUS_1:
        	default:
        		result = _wide_bus(false);
        		if (result == sdcard::SD_OK)
        		{
        			_config.bypass = false;
        			_config.bus_wide = WIDE_BUS_1;
        			_config.clock_divider = sdcard::TRANSFER_DIVIDER;
        			_config.polarity = SDIO_CK_RISING_EDGE;
        			_config.flow_control = false;
        			_config.power_save = false;
      				sdio_config(&_config);
        		}
        		break;
  		}
  	}

  	return(result);
}

sdcard::Result SdcardDriver::_stop_transfer(void)
{
	sdcard::Result result = sdcard::SD_OK;

	_command.index = sdcard::SD_CMD_STOP_TRANSMISSION;
	_command.arg = 0;
	_command.response = RESPONSE_SHORT;
	_command.wait = WAIT_NO;
	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_STOP_TRANSMISSION);

	return(result);
}

sdcard::Result SdcardDriver::_get_card_status(sdcard::CardStatus *cardstatus)
{
	sdcard::Result result = sdcard::SD_OK;
  	uint8_t tmp = 0;

  	result = _send_sd_status((uint32_t *)SDSTATUS_Tab);

  	if (result  != sdcard::SD_OK)
    	return(result);

  	/*!< Byte 0 */
  	tmp = (uint8_t)((SDSTATUS_Tab[0] & 0xC0) >> 6);
  	cardstatus->DAT_BUS_WIDTH = tmp;

  	/*!< Byte 0 */
  	tmp = (uint8_t)((SDSTATUS_Tab[0] & 0x20) >> 5);
  	cardstatus->SECURED_MODE = tmp;

  	/*!< Byte 2 */
  	tmp = (uint8_t)((SDSTATUS_Tab[2] & 0xFF));
  	cardstatus->SD_CARD_TYPE = tmp << 8;

  	/*!< Byte 3 */
  	tmp = (uint8_t)((SDSTATUS_Tab[3] & 0xFF));
  	cardstatus->SD_CARD_TYPE |= tmp;

  	/*!< Byte 4 */
  	tmp = (uint8_t)(SDSTATUS_Tab[4] & 0xFF);
  	cardstatus->SIZE_OF_PROTECTED_AREA = tmp << 24;

  	/*!< Byte 5 */
  	tmp = (uint8_t)(SDSTATUS_Tab[5] & 0xFF);
  	cardstatus->SIZE_OF_PROTECTED_AREA |= tmp << 16;

  	/*!< Byte 6 */
  	tmp = (uint8_t)(SDSTATUS_Tab[6] & 0xFF);
  	cardstatus->SIZE_OF_PROTECTED_AREA |= tmp << 8;

  	/*!< Byte 7 */
  	tmp = (uint8_t)(SDSTATUS_Tab[7] & 0xFF);
  	cardstatus->SIZE_OF_PROTECTED_AREA |= tmp;

  	/*!< Byte 8 */
  	tmp = (uint8_t)((SDSTATUS_Tab[8] & 0xFF));
  	cardstatus->SPEED_CLASS = tmp;

  	/*!< Byte 9 */
  	tmp = (uint8_t)((SDSTATUS_Tab[9] & 0xFF));
  	cardstatus->PERFORMANCE_MOVE = tmp;

  	/*!< Byte 10 */
  	tmp = (uint8_t)((SDSTATUS_Tab[10] & 0xF0) >> 4);
  	cardstatus->AU_SIZE = tmp;

  	/*!< Byte 11 */
  	tmp = (uint8_t)(SDSTATUS_Tab[11] & 0xFF);
  	cardstatus->ERASE_SIZE = tmp << 8;

  	/*!< Byte 12 */
  	tmp = (uint8_t)(SDSTATUS_Tab[12] & 0xFF);
  	cardstatus->ERASE_SIZE |= tmp;

  	/*!< Byte 13 */
  	tmp = (uint8_t)((SDSTATUS_Tab[13] & 0xFC) >> 2);
  	cardstatus->ERASE_TIMEOUT = tmp;

  	/*!< Byte 13 */
  	tmp = (uint8_t)((SDSTATUS_Tab[13] & 0x3));
  	cardstatus->ERASE_OFFSET = tmp;

  	return(result);
}

sdcard::Result SdcardDriver::_send_sd_status(uint32_t *psdstatus)
{
	sdcard::Result result = sdcard::SD_OK;
  	uint32_t count = 0;

  	if (sdio_get_card_status_1() & sdcard::SD_CARD_LOCKED)
  	{
    	result = sdcard::SD_LOCK_UNLOCK_FAILED;
    	return(result);
  	}

  	_command.index = sdcard::SD_CMD_SET_BLOCKLEN;
  	_command.arg = 64;
  	_command.response = RESPONSE_SHORT;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_SET_BLOCKLEN);

  	if (result != sdcard::SD_OK)
    	return(result);

  	_command.index = sdcard::SD_CMD_APP_CMD;
  	_command.arg = (uint32_t)(RCA << 16);
  	_command.response = RESPONSE_SHORT;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);

  	if (result != sdcard::SD_OK)
    return(result);

  	_data_config.direction = CARD_TO_CONTROLLER;
  	_data_config.period = sdcard::SD_DATATIMEOUT;
  	_data_config.size = BLOCK_LENGTH_64_B;
  	_data_config.length = 64;
  	_data_config.mode = BLOCK_DATA_TRANSFER;
  	_data_config.enable = true;
	sdio_data_config(&_data_config);

	_command.index = sdcard::SD_CMD_SD_APP_STAUS;
	_command.arg = 0;
	_command.response = RESPONSE_SHORT;
	_command.wait = WAIT_NO;
	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_SD_APP_STAUS);

  	if (result != sdcard::SD_OK)
    	return(result);

    while (!sdio_get_flag_status(SDIO_RXOVERR) &&
  		   !sdio_get_flag_status(SDIO_DCRCFAIL) &&
  		   !sdio_get_flag_status(SDIO_DTIMEOUT) &&
  		   !sdio_get_flag_status(SDIO_DBCKEND) &&
  		   !sdio_get_flag_status(SDIO_STBITERR))
  	{
    	if (sdio_get_flag_status(SDIO_RXFIFOHF))
    	{
      		for (count = 0; count < 8; count++)
      		{
        		*(psdstatus + count) = sdio_read_fifo_data();
      		}
      		psdstatus += 8;
    	}
  	}

  	if (sdio_get_flag_status(SDIO_DTIMEOUT))
  	{
    	sdio_clear_flag_status(SDIO_DTIMEOUT);
    	result = sdcard::SD_DATA_TIMEOUT;
    	return(result);
  	}

  	else if (sdio_get_flag_status(SDIO_DCRCFAIL))
  	{
  		sdio_clear_flag_status(SDIO_DCRCFAIL);
    	result = sdcard::SD_DATA_CRC_FAIL;
    	return(result);
  	}

  	else if (sdio_get_flag_status(SDIO_RXOVERR))
  	{
    	sdio_clear_flag_status(SDIO_RXOVERR);
    	result = sdcard::SD_RX_OVERRUN;
    	return(result);
  	}

  	else if (sdio_get_flag_status(SDIO_STBITERR))
  	{
    	sdio_clear_flag_status(SDIO_STBITERR);
    	result = sdcard::SD_START_BIT_ERR;
    	return(result);
  	}

  	count = sdcard::SD_DATATIMEOUT;
  	while (sdio_get_flag_status(SDIO_RXDAVL) && (count > 0))
  	{
  		*psdstatus = sdio_read_fifo_data();
    	psdstatus++;
    	count--;
  	}

  	sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);
  	return(result);
}

sdcard::Result SdcardDriver::_send_status(uint32_t *pcardstatus)
{
	sdcard::Result result = sdcard::SD_OK;

	if (pcardstatus == NULL)
	{
		result = sdcard::SD_INVALID_PARAMETER;
		return(result);
	}

	_command.index = sdcard::SD_CMD_SEND_STATUS;
	_command.arg = (uint32_t) RCA << 16;
	_command.response = RESPONSE_SHORT;
	_command.wait = WAIT_NO;
	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_SEND_STATUS);

	if (result != sdcard::SD_OK)
	{
		return(result);
	}

	*pcardstatus = sdio_get_card_status_1();

	return(result);
}

sdcard::State SdcardDriver::_get_state(void)
{
  uint32_t resp1 = 0;

  if(detect()== sdcard::SD_PRESENT)
  {
    if (_send_status(&resp1) != sdcard::SD_OK)
    {
      return sdcard::SD_CARD_ERROR;
    }
    else
    {
      return (sdcard::State)((resp1 >> 9) & 0x0F);
    }
  }
  else
  {
    return sdcard::SD_CARD_ERROR;
  }
}

sdcard::Result SdcardDriver::_cmd_error(void)
{
	sdcard::Result result = sdcard::SD_OK;
	uint32_t timeout;

  	timeout = sdcard::SDIO_CMD0TIMEOUT; /*!< 10000 */

  	while ((timeout > 0) && !sdio_get_flag_status(SDIO_CMDSENT))
  	{
    	timeout--;
  	}

  	if (timeout == 0)
  	{
    	result = sdcard::SD_CMD_RSP_TIMEOUT;
    	return(result);
  	}

  	sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);
  	return (result);
}

sdcard::Result SdcardDriver::_cmd_R7_error(void)
{
  	sdcard::Result result = sdcard::SD_OK;
  	uint32_t timeout = sdcard::SDIO_CMD0TIMEOUT;

  	while (!sdio_get_flag_status(SDIO_CCRCFAIL) &&
  		   !sdio_get_flag_status(SDIO_CMDREND)  &&
  		   !sdio_get_flag_status(SDIO_CTIMEOUT) &&
  		   (timeout > 0))

  	{
    	timeout--;
  	}

  	if ((timeout == 0) || sdio_get_flag_status(SDIO_CTIMEOUT))
  	{
    	result = sdcard::SD_CMD_RSP_TIMEOUT;
    	sdio_clear_flag_status(SDIO_CTIMEOUT);
    	return(result);
  	}

  	if (sdio_get_flag_status(SDIO_CMDREND))
  	{
    	result = sdcard::SD_OK;
    	sdio_clear_flag_status(SDIO_CMDREND);
    	return(result);
  	}

  	return (result);
}

sdcard::Result SdcardDriver::_cmd_R1_error(uint8_t cmd)
{
  	sdcard::Result result = sdcard::SD_OK;
  	uint32_t response_r1;

    while (!sdio_get_flag_status(SDIO_CCRCFAIL) &&
  		   !sdio_get_flag_status(SDIO_CMDREND)  &&
  		   !sdio_get_flag_status(SDIO_CTIMEOUT));


  	if (sdio_get_flag_status(SDIO_CTIMEOUT))
  	{
    	result = sdcard::SD_CMD_RSP_TIMEOUT;
    	sdio_clear_flag_status(SDIO_CTIMEOUT);
    	return(result);
  	}

  	else if (sdio_get_flag_status(SDIO_CCRCFAIL))
  	{
    	result = sdcard::SD_CMD_CRC_FAIL;
    	sdio_clear_flag_status(SDIO_CCRCFAIL);
    	return(result);
  	}

  	if (sdio_get_cmd_response() != cmd)
  	{
    	result = sdcard::SD_ILLEGAL_CMD;
    	return(result);
  	}

  	sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);

  	response_r1 = sdio_get_card_status_1();

  	if ((response_r1 & sdcard::SD_OCR_ERRORBITS) == sdcard::SD_ALLZERO)
 	{
    	return(result);
  	}

  	if (response_r1 & sdcard::SD_OCR_ADDR_OUT_OF_RANGE)
	{
		return(sdcard::SD_ADDR_OUT_OF_RANGE);
	}

  	if (response_r1 & sdcard::SD_OCR_ADDR_MISALIGNED)
  	{
   	 	return(sdcard::SD_ADDR_MISALIGNED);
  	}

  	if (response_r1 & sdcard::SD_OCR_BLOCK_LEN_ERR)
  	{
   	 	return(sdcard::SD_BLOCK_LEN_ERR);
  	}

  	if (response_r1 & sdcard::SD_OCR_ERASE_SEQ_ERR)
  	{
    	return(sdcard::SD_ERASE_SEQ_ERR);
  	}

  	if (response_r1 & sdcard::SD_OCR_BAD_ERASE_PARAM)
  	{
    	return(sdcard::SD_BAD_ERASE_PARAM);
  	}

  	if (response_r1 & sdcard::SD_OCR_WRITE_PROT_VIOLATION)
  	{
    	return(sdcard::SD_WRITE_PROT_VIOLATION);
  	}

  	if (response_r1 & sdcard::SD_OCR_LOCK_UNLOCK_FAILED)
  	{
    	return(sdcard::SD_LOCK_UNLOCK_FAILED);
  	}

  	if (response_r1 & sdcard::SD_OCR_COM_CRC_FAILED)
  	{
    	return(sdcard::SD_COM_CRC_FAILED);
  	}

  	if (response_r1 & sdcard::SD_OCR_ILLEGAL_CMD)
  	{
    	return(sdcard::SD_ILLEGAL_CMD);
  	}

  	if (response_r1 & sdcard::SD_OCR_CARD_ECC_FAILED)
  	{
    	return(sdcard::SD_CARD_ECC_FAILED);
  	}

  	if (response_r1 & sdcard::SD_OCR_CC_ERROR)
  	{
    	return(sdcard::SD_CC_ERROR);
  	}

  	if (response_r1 & sdcard::SD_OCR_GENERAL_UNKNOWN_ERROR)
  	{
    	return(sdcard::SD_GENERAL_UNKNOWN_ERROR);
  	}

  	if (response_r1 & sdcard::SD_OCR_STREAM_READ_UNDERRUN)
  	{
    	return(sdcard::SD_STREAM_READ_UNDERRUN);
  	}

  	if (response_r1 & sdcard::SD_OCR_STREAM_WRITE_OVERRUN)
  	{
    	return(sdcard::SD_STREAM_WRITE_OVERRUN);
  	}

  	if (response_r1 & sdcard::SD_OCR_CID_CSD_OVERWRIETE)
  	{
    	return(sdcard::SD_CID_CSD_OVERWRITE);
  	}

  	if (response_r1 & sdcard::SD_OCR_WP_ERASE_SKIP)
  	{
    	return(sdcard::SD_WP_ERASE_SKIP);
  	}

  	if (response_r1 & sdcard::SD_OCR_CARD_ECC_DISABLED)
  	{
    	return(sdcard::SD_CARD_ECC_DISABLED);
  	}

  	if (response_r1 & sdcard::SD_OCR_ERASE_RESET)
  	{
    	return(sdcard::SD_ERASE_RESET);
  	}

  	if (response_r1 & sdcard::SD_OCR_AKE_SEQ_ERROR)
  	{
    	return(sdcard::SD_AKE_SEQ_ERROR);
  	}

  	return(result);
}

sdcard::Result SdcardDriver::_cmd_R3_error(void)
{
  	sdcard::Result result = sdcard::SD_OK;

  	while (!sdio_get_flag_status(SDIO_CCRCFAIL) &&
  		   !sdio_get_flag_status(SDIO_CMDREND)  &&
  		   !sdio_get_flag_status(SDIO_CTIMEOUT));

  	if (sdio_get_flag_status(SDIO_CTIMEOUT))
 	{
    	result = sdcard::SD_CMD_RSP_TIMEOUT;
    	sdio_clear_flag_status(SDIO_CTIMEOUT);
    	return(result);
  	}

  	sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);
  	return(result);
}

sdcard::Result SdcardDriver::_cmd_R2_error(void) // sdcard_cmd_R2_error(void)
{
  	sdcard::Result result = sdcard::SD_OK;

  	while (!sdio_get_flag_status(SDIO_CCRCFAIL) &&
  		   !sdio_get_flag_status(SDIO_CMDREND)  &&
  		   !sdio_get_flag_status(SDIO_CTIMEOUT));

  	if (sdio_get_flag_status(SDIO_CTIMEOUT))
  	{
    	result = sdcard::SD_CMD_RSP_TIMEOUT;
    	sdio_clear_flag_status(SDIO_CTIMEOUT);
    	return(result);
  	}

  	else if (sdio_get_flag_status(SDIO_CCRCFAIL))
  	{
    	result = sdcard::SD_CMD_CRC_FAIL;
    	sdio_clear_flag_status(SDIO_CCRCFAIL);
    	return(result);
  	}

  	sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);
  	return(result);
}

sdcard::Result SdcardDriver::_cmd_R6_error(uint8_t cmd, uint16_t *prca) //sdcard_cmd_R6_error(uint8_t cmd, uint16_t *prca)
{
  	sdcard::Result result = sdcard::SD_OK;
  	uint32_t response_r1;

  	while (!sdio_get_flag_status(SDIO_CCRCFAIL) &&
  		   !sdio_get_flag_status(SDIO_CMDREND)  &&
  		   !sdio_get_flag_status(SDIO_CTIMEOUT));


  	if (sdio_get_flag_status(SDIO_CTIMEOUT))
  	{
    	result = sdcard::SD_CMD_RSP_TIMEOUT;
    	sdio_clear_flag_status(SDIO_CTIMEOUT);
    	return(result);
  	}

  	else if (sdio_get_flag_status(SDIO_CCRCFAIL))
  	{
    	result = sdcard::SD_CMD_CRC_FAIL;
    	sdio_clear_flag_status(SDIO_CCRCFAIL);
    	return(result);
  	}

  	if (sdio_get_cmd_response() != cmd)
  	{
    	result = sdcard::SD_ILLEGAL_CMD;
    	return(result);
  	}

  	sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);
  	response_r1 = sdio_get_card_status_1();

  	if (sdcard::SD_ALLZERO == (response_r1 & (sdcard::SD_R6_GENERAL_UNKNOWN_ERROR | sdcard::SD_R6_ILLEGAL_CMD | sdcard::SD_R6_COM_CRC_FAILED)))
  	{
    	*prca = (uint16_t) (response_r1 >> 16);
    	return(result);
  	}

  	if (response_r1 & sdcard::SD_R6_GENERAL_UNKNOWN_ERROR)
  	{
    	return(sdcard::SD_GENERAL_UNKNOWN_ERROR);
  	}

  	if (response_r1 & sdcard::SD_R6_ILLEGAL_CMD)
  	{
    	return(sdcard::SD_ILLEGAL_CMD);
  	}

  	if (response_r1 & sdcard::SD_R6_COM_CRC_FAILED)
  	{
    	return(sdcard::SD_COM_CRC_FAILED);
  	}

  	return(result);
}

sdcard::Result SdcardDriver::_wide_bus(bool enable)
{
  	sdcard::Result result = sdcard::SD_OK;
  	uint32_t scr[2] = {0, 0};

  	if (sdio_get_card_status_1() & sdcard::SD_CARD_LOCKED)
  	{
    	result = sdcard::SD_LOCK_UNLOCK_FAILED;
    	return(result);
  	}

  	sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);
  	result = _find_SCR(RCA, scr);

  	if (result != sdcard::SD_OK)
    	return(result);

  	if (enable)
  	{
    	/*!< If requested card supports wide bus operation */
    	if ((scr[1] & sdcard::SD_WIDE_BUS_SUPPORT) != sdcard::SD_ALLZERO)
    	{
    		_command.index = sdcard::SD_CMD_APP_CMD;
    		_command.arg = (uint32_t)(RCA << 16);
    		_command.response = RESPONSE_SHORT;
    		_command.wait = WAIT_NO;
    		_command.enable_CPSM = true;
    	  	sdio_send_command(&_command);

      		result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);

  			if (result != sdcard::SD_OK)
    			return(result);

  			_command.index = sdcard::SD_CMD_APP_SD_SET_BUSWIDTH;
  			_command.arg = 0x2;
  			_command.response = RESPONSE_SHORT;
  			_command.wait = WAIT_NO;
  			_command.enable_CPSM = true;
    	  	sdio_send_command(&_command);

      		result = _cmd_R1_error(sdcard::SD_CMD_APP_SD_SET_BUSWIDTH);

      		if (result != sdcard::SD_OK)
    			return(result);

      		return(result);
    	}

    	else
    	{
      		result = sdcard::SD_REQUEST_NOT_APPLICABLE;
      		return(result);
    	}
  	}

 	else
  	{
    /*!< If requested card supports 1 bit mode operation */
	    if ((scr[1] & sdcard::SD_SINGLE_BUS_SUPPORT) != sdcard::SD_ALLZERO)
	    {
	    	_command.index = sdcard::SD_CMD_APP_CMD;
	    	_command.arg = (uint32_t)(RCA << 16);
	    	_command.response = RESPONSE_SHORT;
	    	_command.wait = WAIT_NO;
    	  	_command.enable_CPSM = true;
    	  	sdio_send_command(&_command);

      		result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);

  			if (result != sdcard::SD_OK)
    			return(result);

  			_command.index = sdcard::SD_CMD_APP_SD_SET_BUSWIDTH;
  			_command.arg = 0;
  			_command.response = RESPONSE_SHORT;
  			_command.wait = WAIT_NO;
  			_command.enable_CPSM = true;
    	  	sdio_send_command(&_command);

    		result = _cmd_R1_error(sdcard::SD_CMD_APP_SD_SET_BUSWIDTH);

    		if (result != sdcard::SD_OK)
    			return(result);

      		return(result);
    	}

    	else
    	{
      		result = sdcard::SD_REQUEST_NOT_APPLICABLE;
      		return(result);
    	}
  	}
}

sdcard::Result SdcardDriver::_is_busy(uint8_t *pstatus)
{
	sdcard::Result result = sdcard::SD_OK;
	uint32_t respR1 = 0;

	_command.index = sdcard::SD_CMD_SEND_STATUS;
	_command.arg = (uint32_t)(RCA << 16);
	_command.response = RESPONSE_SHORT;
	_command.wait = WAIT_NO;
	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

	while (!sdio_get_flag_status(SDIO_CCRCFAIL) &&
  		   !sdio_get_flag_status(SDIO_CMDREND) &&
  		   !sdio_get_flag_status(SDIO_CTIMEOUT));

  	if (sdio_get_flag_status(SDIO_CTIMEOUT))
  	{
    	result = sdcard::SD_CMD_RSP_TIMEOUT;
    	sdio_clear_flag_status(SDIO_CTIMEOUT);
    	return(result);
  	}

  	else if (sdio_get_flag_status(SDIO_CCRCFAIL))
  	{
    	result = sdcard::SD_CMD_CRC_FAIL;
    	sdio_clear_flag_status(SDIO_CCRCFAIL);
    	return(result);
  	}

  	if ((uint32_t)sdio_get_cmd_response() != sdcard::SD_CMD_SEND_STATUS)
  	{
    	result = sdcard::SD_ILLEGAL_CMD;
    	return(result);
  	}

  	sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);

  	respR1 = sdio_get_card_status_1();
  	*pstatus = (uint8_t) ((respR1 >> 9) & 0x0000000F);

  	if ((respR1 & sdcard::SD_OCR_ERRORBITS) == sdcard::SD_ALLZERO)
  	{
    	return(result);
  	}

  	if (respR1 & sdcard::SD_OCR_ADDR_OUT_OF_RANGE)
  	{
    	return(sdcard::SD_ADDR_OUT_OF_RANGE);
  	}

  	if (respR1 & sdcard::SD_OCR_ADDR_MISALIGNED)
  	{
    	return(sdcard::SD_ADDR_MISALIGNED);
  	}

  	if (respR1 & sdcard::SD_OCR_BLOCK_LEN_ERR)
  	{
    	return(sdcard::SD_BLOCK_LEN_ERR);
  	}

  	if (respR1 & sdcard::SD_OCR_ERASE_SEQ_ERR)
  	{
    	return(sdcard::SD_ERASE_SEQ_ERR);
  	}

  	if (respR1 & sdcard::SD_OCR_BAD_ERASE_PARAM)
  	{
    	return(sdcard::SD_BAD_ERASE_PARAM);
  	}

  	if (respR1 & sdcard::SD_OCR_WRITE_PROT_VIOLATION)
  	{
    	return(sdcard::SD_WRITE_PROT_VIOLATION);
  	}

  	if (respR1 & sdcard::SD_OCR_LOCK_UNLOCK_FAILED)
  	{
    	return(sdcard::SD_LOCK_UNLOCK_FAILED);
  	}

  	if (respR1 & sdcard::SD_OCR_COM_CRC_FAILED)
  	{
    	return(sdcard::SD_COM_CRC_FAILED);
  	}

  	if (respR1 & sdcard::SD_OCR_ILLEGAL_CMD)
  	{
    	return(sdcard::SD_ILLEGAL_CMD);
  	}

  	if (respR1 & sdcard::SD_OCR_CARD_ECC_FAILED)
  	{
    	return(sdcard::SD_CARD_ECC_FAILED);
  	}

  	if (respR1 & sdcard::SD_OCR_CC_ERROR)
  	{
    	return(sdcard::SD_CC_ERROR);
  	}

  	if (respR1 & sdcard::SD_OCR_GENERAL_UNKNOWN_ERROR)
  	{
    	return(sdcard::SD_GENERAL_UNKNOWN_ERROR);
  	}

  	if (respR1 & sdcard::SD_OCR_STREAM_READ_UNDERRUN)
  	{
    	return(sdcard::SD_STREAM_READ_UNDERRUN);
  	}

  	if (respR1 & sdcard::SD_OCR_STREAM_WRITE_OVERRUN)
  	{
    	return(sdcard::SD_STREAM_WRITE_OVERRUN);
  	}

  	if (respR1 & sdcard::SD_OCR_CID_CSD_OVERWRIETE)
  	{
    	return(sdcard::SD_CID_CSD_OVERWRITE);
  	}

  	if (respR1 & sdcard::SD_OCR_WP_ERASE_SKIP)
  	{
    	return(sdcard::SD_WP_ERASE_SKIP);
  	}

  	if (respR1 & sdcard::SD_OCR_CARD_ECC_DISABLED)
  	{
    	return(sdcard::SD_CARD_ECC_DISABLED);
  	}

  	if (respR1 & sdcard::SD_OCR_ERASE_RESET)
  	{
    	return(sdcard::SD_ERASE_RESET);
  	}

  	if (respR1 & sdcard::SD_OCR_AKE_SEQ_ERROR)
  	{
    	return(sdcard::SD_AKE_SEQ_ERROR);
  	}

  	return(result);
}

sdcard::Result SdcardDriver::_find_SCR(uint16_t rca, uint32_t *pscr)
{
	uint32_t index = 0;
  	sdcard::Result result = sdcard::SD_OK;
  	uint32_t tempscr[2] = {0, 0};

  	_command.index = sdcard::SD_CMD_SET_BLOCKLEN;
  	_command.arg = 8;
  	_command.response = RESPONSE_SHORT;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_SET_BLOCKLEN);

  	if (result != sdcard::SD_OK)
    	return(result);

  	_command.index = sdcard::SD_CMD_APP_CMD;
  	_command.arg = (uint32_t)(RCA << 16);
  	_command.response = RESPONSE_SHORT;
  	_command.wait = WAIT_NO;
  	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

  	result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);

  	if (result != sdcard::SD_OK)
    	return(result);

  	_data_config.direction = CARD_TO_CONTROLLER;
  	_data_config.period = sdcard::SD_DATATIMEOUT;
  	_data_config.size = BLOCK_LENGTH_8_B;
  	_data_config.length = 8;
  	_data_config.mode = BLOCK_DATA_TRANSFER;
  	_data_config.enable = true;
	sdio_data_config(&_data_config);

	_command.index = sdcard::SD_CMD_SD_APP_SEND_SCR;
	_command.arg = 0;
	_command.response = RESPONSE_SHORT;
	_command.wait = WAIT_NO;
	_command.enable_CPSM = true;
  	sdio_send_command(&_command);

	result = _cmd_R1_error(sdcard::SD_CMD_SD_APP_SEND_SCR);

  	if (result != sdcard::SD_OK)
    	return(result);

   	while (!sdio_get_flag_status(SDIO_RXOVERR) &&
  		   !sdio_get_flag_status(SDIO_DCRCFAIL) &&
  		   !sdio_get_flag_status(SDIO_DTIMEOUT) &&
  		   !sdio_get_flag_status(SDIO_DBCKEND) &&
  		   !sdio_get_flag_status(SDIO_STBITERR))
  	{
    	if (sdio_get_flag_status(SDIO_RXDAVL))
    	{
      		*(tempscr + index) = sdio_read_fifo_data();
      		index++;
    	}
  	}

  	if (sdio_get_flag_status(SDIO_DTIMEOUT))
  	{
    	sdio_clear_flag_status(SDIO_DTIMEOUT);
    	result = sdcard::SD_DATA_TIMEOUT;
    	return(result);
  	}

  	else if (sdio_get_flag_status(SDIO_DCRCFAIL))
  	{
    	sdio_clear_flag_status(SDIO_DCRCFAIL);
    	result = sdcard::SD_DATA_CRC_FAIL;
    	return(result);
  	}
  	else if (sdio_get_flag_status(SDIO_RXOVERR))
  	{
    	sdio_clear_flag_status(SDIO_RXOVERR);
    	result = sdcard::SD_RX_OVERRUN;
    	return(result);
  	}
  	else if (sdio_get_flag_status(SDIO_STBITERR))
  	{
    	sdio_clear_flag_status(SDIO_STBITERR);
    	result = sdcard::SD_START_BIT_ERR;
    	return(result);
  	}

  	sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);

  	*(pscr + 1) = ((tempscr[0] & sdcard::SD_0TO7BITS) << 24) | ((tempscr[0] & sdcard::SD_8TO15BITS) << 8) | ((tempscr[0] & sdcard::SD_16TO23BITS) >> 8) | ((tempscr[0] & sdcard::SD_24TO31BITS) >> 24);

  	*(pscr) = ((tempscr[1] & sdcard::SD_0TO7BITS) << 24) | ((tempscr[1] & sdcard::SD_8TO15BITS) << 8) | ((tempscr[1] & sdcard::SD_16TO23BITS) >> 8) | ((tempscr[1] & sdcard::SD_24TO31BITS) >> 24);

  	return(result);
}

void SdcardDriver::_sdio_config()
{
	GPIO_CPP_Extension::Pinout _pinout[] = {config::SDIO_D0, config::SDIO_D1, config::SDIO_D2,
			                                config::SDIO_D3, config::SDIO_CM, config::SDIO_CK};

	GPIO_CPP_Extension::GPIO_ext _gpio;

	for (uint8_t pin = 0; pin < config::SDIO_PIN_COUNT; pin++) {
		_gpio.init(_pinout[pin]);
		_gpio.set_af(config::SDIO_AF);

		if (pin != 5) {
			_gpio.mode_setup(GPIO_CPP_Extension::Mode::ALTERNATE_FUNCTION,
					         GPIO_CPP_Extension::PullMode::PULL_UP);
		}

		else {
			_gpio.mode_setup(GPIO_CPP_Extension::Mode::ALTERNATE_FUNCTION,
					         GPIO_CPP_Extension::PullMode::NO_PULL);
		}

		_gpio.set_output_options(GPIO_CPP_Extension::OutputType::PUSH_PULL,
				                 GPIO_CPP_Extension::Speed::FAST_50MHz);
	}

	_sd_det.init(config::SD_DET);
	_sd_det.mode_setup(GPIO_CPP_Extension::Mode::INPUT,
			           GPIO_CPP_Extension::PullMode::PULL_UP);

	nvic_set_priority(config::SDIO_IRQ, config::SDIO_IRQ_PRIORITY);
	nvic_set_priority(config::SDIO_DMA_IRQ, config::SDIO_DMA_IRQ_PRIORITY);

	nvic_enable_irq(config::SDIO_IRQ);
	nvic_enable_irq(config::SDIO_DMA_IRQ);
}

void SdcardDriver::_dma_tx_config(uint32_t *buffer, uint32_t size)
{
	dma_clear_interrupt_flags(config::SDIO_DMA, config::SDIO_DMA_STREAM,
			                 (DMA_HISR_FEIF4 | DMA_HISR_FEIF4 |
							  DMA_HISR_FEIF4 | DMA_HISR_FEIF4 | DMA_HISR_FEIF4));

	dma_disable_stream(config::SDIO_DMA, config::SDIO_DMA_STREAM);
	dma_stream_reset(config::SDIO_DMA, config::SDIO_DMA_STREAM);
	dma_channel_select(config::SDIO_DMA, config::SDIO_DMA_STREAM, config::SDIO_DMA_CHANNEL);
	dma_set_peripheral_address(config::SDIO_DMA, config::SDIO_DMA_STREAM, (uint32_t)&SDIO_FIFO);
	dma_set_memory_address(config::SDIO_DMA, config::SDIO_DMA_STREAM, (uint32_t)buffer);
	dma_set_transfer_mode(config::SDIO_DMA, config::SDIO_DMA_STREAM, DMA_SxCR_DIR_MEM_TO_PERIPHERAL);
	dma_set_number_of_data(config::SDIO_DMA, config::SDIO_DMA_STREAM, size);
	dma_disable_peripheral_increment_mode(config::SDIO_DMA, config::SDIO_DMA_STREAM);
	dma_enable_memory_increment_mode(config::SDIO_DMA, config::SDIO_DMA_STREAM);
	dma_set_peripheral_size(config::SDIO_DMA, config::SDIO_DMA_STREAM, DMA_SxCR_PSIZE_32BIT);
	dma_set_memory_size(config::SDIO_DMA, config::SDIO_DMA_STREAM, DMA_SxCR_MSIZE_32BIT);
	dma_set_priority(config::SDIO_DMA, config::SDIO_DMA_STREAM, DMA_SxCR_PL_VERY_HIGH);
	dma_enable_fifo_mode(config::SDIO_DMA, config::SDIO_DMA_STREAM);
	dma_set_fifo_threshold(config::SDIO_DMA, config::SDIO_DMA_STREAM, DMA_SxFCR_FS_LT_4_4_FULL);
	dma_set_memory_burst(config::SDIO_DMA, config::SDIO_DMA_STREAM, DMA_SxCR_MBURST_INCR4);
	dma_set_peripheral_burst(config::SDIO_DMA, config::SDIO_DMA_STREAM, DMA_SxCR_PBURST_INCR4);
	dma_enable_transfer_complete_interrupt(config::SDIO_DMA, config::SDIO_DMA_STREAM);
	dma_set_peripheral_flow_control(config::SDIO_DMA, config::SDIO_DMA_STREAM);
	dma_enable_stream(config::SDIO_DMA, config::SDIO_DMA_STREAM);
}

void SdcardDriver::_dma_rx_config(uint32_t *buffer, uint32_t size)
{
	dma_clear_interrupt_flags(config::SDIO_DMA, config::SDIO_DMA_STREAM,
			                  (DMA_HISR_FEIF4 | DMA_HISR_FEIF4 |
							   DMA_HISR_FEIF4 | DMA_HISR_FEIF4 | DMA_HISR_FEIF4));

	dma_disable_stream(config::SDIO_DMA, config::SDIO_DMA_STREAM);
	dma_stream_reset(config::SDIO_DMA, config::SDIO_DMA_STREAM);
	dma_channel_select(config::SDIO_DMA, config::SDIO_DMA_STREAM, config::SDIO_DMA_CHANNEL);
	dma_set_peripheral_address(config::SDIO_DMA, config::SDIO_DMA_STREAM, (uint32_t)&SDIO_FIFO);
	dma_set_memory_address(config::SDIO_DMA, config::SDIO_DMA_STREAM, (uint32_t)buffer);
	dma_set_transfer_mode(config::SDIO_DMA, config::SDIO_DMA_STREAM, DMA_SxCR_DIR_PERIPHERAL_TO_MEM);
	dma_set_number_of_data(config::SDIO_DMA, config::SDIO_DMA_STREAM, size);
	dma_disable_peripheral_increment_mode(config::SDIO_DMA, config::SDIO_DMA_STREAM);
	dma_enable_memory_increment_mode(config::SDIO_DMA, config::SDIO_DMA_STREAM);
	dma_set_peripheral_size(config::SDIO_DMA, config::SDIO_DMA_STREAM, DMA_SxCR_PSIZE_32BIT);
	dma_set_memory_size(config::SDIO_DMA, config::SDIO_DMA_STREAM, DMA_SxCR_MSIZE_32BIT);
	dma_set_priority(config::SDIO_DMA, config::SDIO_DMA_STREAM, DMA_SxCR_PL_VERY_HIGH);
	dma_enable_fifo_mode(config::SDIO_DMA, config::SDIO_DMA_STREAM);
	dma_set_fifo_threshold(config::SDIO_DMA, config::SDIO_DMA_STREAM, DMA_SxFCR_FS_LT_4_4_FULL);
	dma_set_memory_burst(config::SDIO_DMA, config::SDIO_DMA_STREAM, DMA_SxCR_MBURST_INCR4);
	dma_set_peripheral_burst(config::SDIO_DMA, config::SDIO_DMA_STREAM, DMA_SxCR_PBURST_INCR4);
	dma_enable_transfer_complete_interrupt(config::SDIO_DMA, config::SDIO_DMA_STREAM);
	dma_set_peripheral_flow_control(config::SDIO_DMA, config::SDIO_DMA_STREAM);
	dma_enable_stream(config::SDIO_DMA, config::SDIO_DMA_STREAM);
}

} /* namespace sdcard */

} /* namespace hw */

extern "C"
{
	void sdio_isr()
	{
		hw::sdcard::SdcardDriver::sdio_irq_proc();
	}

	void dma2_stream3_isr(void)
	{
		hw::sdcard::SdcardDriver::dma_irq_proc();
	}
}
