/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HW_SDCARD_DEFINITIONS_H_
#define HW_SDCARD_DEFINITIONS_H_

namespace hw {

namespace sdcard {

typedef enum : uint8_t
{
	SD_OK,
	SD_CMD_CRC_FAIL,
	SD_DATA_CRC_FAIL,
	SD_CMD_RSP_TIMEOUT,
	SD_DATA_TIMEOUT,
	SD_TX_UNDERRUN,
	SD_RX_OVERRUN,
	SD_START_BIT_ERR,
	SD_CMD_OUT_OF_RANGE,
	SD_ADDR_MISALIGNED,
	SD_BLOCK_LEN_ERR,
	SD_ERASE_SEQ_ERR,
	SD_BAD_ERASE_PARAM,
	SD_WRITE_PROT_VIOLATION,
	SD_LOCK_UNLOCK_FAILED,
	SD_COM_CRC_FAILED,
	SD_ILLEGAL_CMD,
	SD_CARD_ECC_FAILED,
	SD_CC_ERROR,
	SD_GENERAL_UNKNOWN_ERROR,
	SD_STREAM_READ_UNDERRUN,
	SD_STREAM_WRITE_OVERRUN,
	SD_CID_CSD_OVERWRITE,
	SD_WP_ERASE_SKIP,
	SD_CARD_ECC_DISABLED,
	SD_ERASE_RESET,
	SD_AKE_SEQ_ERROR,
	SD_INVALID_VOLTRANGE,
	SD_ADDR_OUT_OF_RANGE,
	SD_SWITCH_ERROR,
	SD_SDIO_DISABLED,
	SD_SDIO_FUNCTION_BUSY,
	SD_SDIO_FUNCTION_FAILED,
	SD_SDIO_UNKNOWN_FUNCTION,
	SD_INTERNAL_ERROR,
	SD_NOT_CONFIGURED,
	SD_REQUEST_PENDING,
	SD_REQUEST_NOT_APPLICABLE,
	SD_INVALID_PARAMETER,
	SD_UNSUPPORTED_FEATURE,
	SD_UNSUPPORTED_HW,
	SD_ERROR,
} Result;

typedef enum : uint8_t
{
	SD_TRANSFER_OK,
	SD_TRANSFER_BUSY,
	SD_TRANSFER_ERROR
} TransferState;

typedef enum : uint32_t
{
	SD_CARD_READY                  = ((uint32_t)0x00000001),
	SD_CARD_IDENTIFICATION         = ((uint32_t)0x00000002),
	SD_CARD_STANDBY                = ((uint32_t)0x00000003),
	SD_CARD_TRANSFER               = ((uint32_t)0x00000004),
	SD_CARD_SENDING                = ((uint32_t)0x00000005),
	SD_CARD_RECEIVING              = ((uint32_t)0x00000006),
	SD_CARD_PROGRAMMING            = ((uint32_t)0x00000007),
	SD_CARD_DISCONNECTED           = ((uint32_t)0x00000008),
	SD_CARD_ERROR                  = ((uint32_t)0x000000FF),
} State;

typedef struct
{
	uint8_t  CSDStruct;
	uint8_t  SysSpecVersion;
	uint8_t  Reserved1;
	uint8_t  TAAC;
	uint8_t  NSAC;
	uint8_t  MaxBusClkFrec;
	uint16_t CardComdClasses;
	uint8_t  RdBlockLen;
	uint8_t  PartBlockRead;
	uint8_t  WrBlockMisalign;
	uint8_t  RdBlockMisalign;
	uint8_t  DSRImpl;
	uint8_t  Reserved2;
	uint32_t DeviceSize;
	uint8_t  MaxRdCurrentVDDMin;
	uint8_t  MaxRdCurrentVDDMax;
	uint8_t  MaxWrCurrentVDDMin;
	uint8_t  MaxWrCurrentVDDMax;
	uint8_t  DeviceSizeMul;
	uint8_t  EraseGrSize;
	uint8_t  EraseGrMul;
	uint8_t  WrProtectGrSize;
	uint8_t  WrProtectGrEnable;
	uint8_t  ManDeflECC;
	uint8_t  WrSpeedFact;
	uint8_t  MaxWrBlockLen;
	uint8_t  WriteBlockPaPartial;
	uint8_t  Reserved3;
	uint8_t  ContentProtectAppli;
	uint8_t  FileFormatGrouop;
	uint8_t  CopyFlag;
	uint8_t  PermWrProtect;
	uint8_t  TempWrProtect;
	uint8_t  FileFormat;
	uint8_t  ECC;
	uint8_t  CSD_CRC;
	uint8_t  Reserved4;
} CSD;

typedef struct
{
	uint8_t  ManufacturerID;
	uint16_t OEM_AppliID;
	uint32_t ProdName1;
	uint8_t  ProdName2;
	uint8_t  ProdRev;
	uint32_t ProdSN;
	uint8_t  Reserved1;
	uint16_t ManufactDate;
	uint8_t  CID_CRC;
	uint8_t  Reserved2;
} CID;

typedef struct
{
	uint8_t DAT_BUS_WIDTH;
	uint8_t SECURED_MODE;
	uint16_t SD_CARD_TYPE;
	uint32_t SIZE_OF_PROTECTED_AREA;
	uint8_t SPEED_CLASS;
	uint8_t PERFORMANCE_MOVE;
	uint8_t AU_SIZE;
	uint16_t ERASE_SIZE;
	uint8_t ERASE_TIMEOUT;
	uint8_t ERASE_OFFSET;
} CardStatus;

typedef struct
{
	CSD SD_csd;
	CID SD_cid;
	uint64_t CardCapacity;
	uint32_t CardBlockSize;
	uint16_t RCA;
	uint8_t CardType;
} CardInfo;

constexpr uint8_t SD_CMD_GO_IDLE_STATE                       = 0;
constexpr uint8_t SD_CMD_SEND_OP_COND                        = 1;
constexpr uint8_t SD_CMD_ALL_SEND_CID                        = 2;
constexpr uint8_t SD_CMD_SET_REL_ADDR                        = 3;
constexpr uint8_t SD_CMD_SET_DSR                             = 4;
constexpr uint8_t SD_CMD_SDIO_SEN_OP_COND                    = 5;
constexpr uint8_t SD_CMD_HS_SWITCH                           = 6;
constexpr uint8_t SD_CMD_SEL_DESEL_CARD                      = 7;
constexpr uint8_t SD_CMD_HS_SEND_EXT_CSD                     = 8;
constexpr uint8_t SD_CMD_SEND_CSD                            = 9;
constexpr uint8_t SD_CMD_SEND_CID                            = 10;
constexpr uint8_t SD_CMD_READ_DAT_UNTIL_STOP                 = 11;
constexpr uint8_t SD_CMD_STOP_TRANSMISSION                   = 12;
constexpr uint8_t SD_CMD_SEND_STATUS                         = 13;
constexpr uint8_t SD_CMD_HS_BUSTEST_READ                     = 14;
constexpr uint8_t SD_CMD_GO_INACTIVE_STATE                   = 15;
constexpr uint8_t SD_CMD_SET_BLOCKLEN                        = 16;
constexpr uint8_t SD_CMD_READ_SINGLE_BLOCK                   = 17;
constexpr uint8_t SD_CMD_READ_MULT_BLOCK                     = 18;
constexpr uint8_t SD_CMD_HS_BUSTEST_WRITE                    = 19;
constexpr uint8_t SD_CMD_WRITE_DAT_UNTIL_STOP                = 20;
constexpr uint8_t SD_CMD_SET_BLOCK_COUNT                     = 23;
constexpr uint8_t SD_CMD_WRITE_SINGLE_BLOCK                  = 24;
constexpr uint8_t SD_CMD_WRITE_MULT_BLOCK                    = 25;
constexpr uint8_t SD_CMD_PROG_CID                            = 26;
constexpr uint8_t SD_CMD_PROG_CSD                            = 27;
constexpr uint8_t SD_CMD_SET_WRITE_PROT                      = 28;
constexpr uint8_t SD_CMD_CLR_WRITE_PROT                      = 29;
constexpr uint8_t SD_CMD_SEND_WRITE_PROT                     = 30;
constexpr uint8_t SD_CMD_SD_ERASE_GRP_START                  = 32;
constexpr uint8_t SD_CMD_SD_ERASE_GRP_END                    = 33;
constexpr uint8_t SD_CMD_ERASE_GRP_START                     = 35;
constexpr uint8_t SD_CMD_ERASE_GRP_END                       = 36;
constexpr uint8_t SD_CMD_ERASE                               = 38;
constexpr uint8_t SD_CMD_FAST_IO                             = 39;
constexpr uint8_t SD_CMD_GO_IRQ_STATE                        = 40;
constexpr uint8_t SD_CMD_LOCK_UNLOCK                         = 42;
constexpr uint8_t SD_CMD_APP_CMD                             = 55;
constexpr uint8_t SD_CMD_GEN_CMD                             = 56;
constexpr uint8_t SD_CMD_NO_CMD                              = 64;

constexpr uint8_t SD_CMD_APP_SD_SET_BUSWIDTH                 = 6;
constexpr uint8_t SD_CMD_SD_APP_STAUS                        = 13;
constexpr uint8_t SD_CMD_SD_APP_SEND_NUM_WRITE_BLOCKS        = 22;
constexpr uint8_t SD_CMD_SD_APP_OP_COND                      = 41;
constexpr uint8_t SD_CMD_SD_APP_SET_CLR_CARD_DETECT          = 42;
constexpr uint8_t SD_CMD_SD_APP_SEND_SCR                     = 51;
constexpr uint8_t SD_CMD_SDIO_RW_DIRECT                      = 52;
constexpr uint8_t SD_CMD_SDIO_RW_EXTENDED                    = 53;

constexpr uint8_t SD_CMD_SD_APP_GET_MKB                      = 43;
constexpr uint8_t SD_CMD_SD_APP_GET_MID                      = 44;
constexpr uint8_t SD_CMD_SD_APP_SET_CER_RN1                  = 45;
constexpr uint8_t SD_CMD_SD_APP_GET_CER_RN2                  = 46;
constexpr uint8_t SD_CMD_SD_APP_SET_CER_RES2                 = 47;
constexpr uint8_t SD_CMD_SD_APP_GET_CER_RES1                 = 48;
constexpr uint8_t SD_CMD_SD_APP_SECURE_READ_MULTIPLE_BLOCK   = 18;
constexpr uint8_t SD_CMD_SD_APP_SECURE_WRITE_MULTIPLE_BLOCK  = 25;
constexpr uint8_t SD_CMD_SD_APP_SECURE_ERASE                 = 38;
constexpr uint8_t SD_CMD_SD_APP_CHANGE_SECURE_AREA           = 49;
constexpr uint8_t SD_CMD_SD_APP_SECURE_WRITE_MKB             = 48;

constexpr uint8_t SD_PRESENT                                 = 0x01;
constexpr uint8_t SD_NOT_PRESENT                             = 0x00;

constexpr uint32_t SDIO_STD_CAPACITY_SD_CARD_V1_1            = 0x00000000;
constexpr uint32_t SDIO_STD_CAPACITY_SD_CARD_V2_0            = 0x00000001;
constexpr uint32_t SDIO_HIGH_CAPACITY_SD_CARD                = 0x00000002;
constexpr uint32_t SDIO_MULTIMEDIA_CARD                      = 0x00000003;
constexpr uint32_t SDIO_SECURE_DIGITAL_IO_CARD               = 0x00000004;
constexpr uint32_t SDIO_HIGH_SPEED_MULTIMEDIA_CARD           = 0x00000005;
constexpr uint32_t SDIO_SECURE_DIGITAL_IO_COMBO_CARD         = 0x00000006;
constexpr uint32_t SDIO_HIGH_CAPACITY_MMC_CARD               = 0x00000007;

constexpr uint32_t SDIO_STATIC_FLAGS               			 = 0x000005FF;
constexpr uint32_t SDIO_CMD0TIMEOUT                			 = 0x00010000;

constexpr uint32_t SD_OCR_ADDR_OUT_OF_RANGE        			 = 0x80000000;
constexpr uint32_t SD_OCR_ADDR_MISALIGNED         			 = 0x40000000;
constexpr uint32_t SD_OCR_BLOCK_LEN_ERR            			 = 0x20000000;
constexpr uint32_t SD_OCR_ERASE_SEQ_ERR            			 = 0x10000000;
constexpr uint32_t SD_OCR_BAD_ERASE_PARAM          			 = 0x08000000;
constexpr uint32_t SD_OCR_WRITE_PROT_VIOLATION     			 = 0x04000000;
constexpr uint32_t SD_OCR_LOCK_UNLOCK_FAILED       			 = 0x01000000;
constexpr uint32_t SD_OCR_COM_CRC_FAILED           			 = 0x00800000;
constexpr uint32_t SD_OCR_ILLEGAL_CMD              			 = 0x00400000;
constexpr uint32_t SD_OCR_CARD_ECC_FAILED          			 = 0x00200000;
constexpr uint32_t SD_OCR_CC_ERROR                 			 = 0x00100000;
constexpr uint32_t SD_OCR_GENERAL_UNKNOWN_ERROR    			 = 0x00080000;
constexpr uint32_t SD_OCR_STREAM_READ_UNDERRUN     			 = 0x00040000;
constexpr uint32_t SD_OCR_STREAM_WRITE_OVERRUN     			 = 0x00020000;
constexpr uint32_t SD_OCR_CID_CSD_OVERWRIETE       			 = 0x00010000;
constexpr uint32_t SD_OCR_WP_ERASE_SKIP            			 = 0x00008000;
constexpr uint32_t SD_OCR_CARD_ECC_DISABLED        			 = 0x00004000;
constexpr uint32_t SD_OCR_ERASE_RESET              			 = 0x00002000;
constexpr uint32_t SD_OCR_AKE_SEQ_ERROR            			 = 0x00000008;
constexpr uint32_t SD_OCR_ERRORBITS                			 = 0xFDFFE008;

constexpr uint32_t SD_R6_GENERAL_UNKNOWN_ERROR     			 = 0x00002000;
constexpr uint32_t SD_R6_ILLEGAL_CMD               			 = 0x00004000;
constexpr uint32_t SD_R6_COM_CRC_FAILED            			 = 0x00008000;

constexpr uint32_t SD_VOLTAGE_WINDOW_SD            			 = 0x80100000;
constexpr uint32_t SD_HIGH_CAPACITY                			 = 0x40000000;
constexpr uint32_t SD_STD_CAPACITY                 			 = 0x00000000;
constexpr uint32_t SD_CHECK_PATTERN                			 = 0x000001AA;

constexpr uint32_t SD_MAX_VOLT_TRIAL               			 = 0x0000FFFF;
constexpr uint32_t SD_ALLZERO                      			 = 0x00000000;

constexpr uint32_t SD_WIDE_BUS_SUPPORT             			 = 0x00040000;
constexpr uint32_t SD_SINGLE_BUS_SUPPORT           			 = 0x00010000;
constexpr uint32_t SD_CARD_LOCKED                  			 = 0x02000000;

constexpr uint32_t SD_DATATIMEOUT                  			 = 0xFFFFFFFF;
constexpr uint32_t SD_0TO7BITS                     			 = 0x000000FF;
constexpr uint32_t SD_8TO15BITS                    			 = 0x0000FF00;
constexpr uint32_t SD_16TO23BITS                   			 = 0x00FF0000;
constexpr uint32_t SD_24TO31BITS                  			 = 0xFF000000;
constexpr uint32_t SD_MAX_DATA_LENGTH              			 = 0x01FFFFFF;

constexpr uint32_t SD_HALFFIFO                     			 = 0x00000008;
constexpr uint32_t SD_HALFFIFOBYTES                			 = 0x00000020;

constexpr uint32_t SD_CCCC_LOCK_UNLOCK             			 = 0x00000080;
constexpr uint32_t SD_CCCC_WRITE_PROT             			 = 0x00000040;
constexpr uint32_t SD_CCCC_ERASE                   			 = 0x00000020;

constexpr uint32_t SDIO_SEND_IF_COND               			 = 0x00000008;

constexpr uint8_t INIT_DIVIDER                               = 118;
constexpr uint8_t TRANSFER_DIVIDER                           = 0;

constexpr uint32_t BLOCK_SIZE                           	 = 512;

} /* namespace sdcard */

} /* namespace hw */

#endif /* HW_SDCARD_H_ */
