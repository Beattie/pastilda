/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HW_FILESYSTEM_H_
#define HW_FILESYSTEM_H_

#include <thread.hpp>
#include <critical.hpp>
#include <string.h>

#include <fatfs/ff.h>
#include <config/HwConfig.h>
#include <lang/StringFixed.hpp>
#include <libopencmsis/core_cm3.h>

#include "SdcardDriver.h"

namespace hw {

namespace rtos = cpp_freertos;

class FileSystem {

public:
	using FatFs = FATFS;
	using Result = FRESULT;
	using File = FIL;
	using FileInfo = FILINFO;
	using Directory = DIR;
	using SdCard = sdcard::SdcardDriver;
	using SdCardResult = sdcard::Result;

	static FileSystem& Instance()
	{
		static FileSystem* instance = new FileSystem();
		return *instance;
	}

	static int msd_read(uint32_t block, uint8_t *buffer);
	static int msd_write(uint32_t block, const uint8_t *buffer);
	static int msd_blocks(void);

	static Result open_file_to_read(File *file, const char *name);
	static Result open_file_to_write(File *file, const char *name);
	static Result close_file(File *file);
	static Result read_next_file_chunk(File *file, void *buffer, uint32_t size);
	static Result write_next_file_chunk(File *file, void *buffer, uint32_t size);
	static uint32_t get_file_tell(File *file);

private:
	static FatFs _fs;

	FileSystem();
	~FileSystem() = delete;
	FileSystem(const FileSystem& root);
	FileSystem& operator=(const FileSystem&);
};

} /* namespace hw */

#endif /* HW_FILESYSTEM_H_ */
