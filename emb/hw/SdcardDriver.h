/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HW_SDCARD_H_
#define HW_SDCARD_H_

#include <libopencm3/stm32/sdio.h>
#include <config/HwConfig.h>

#include "SdcardDefinitions.h"

namespace hw {

namespace sdcard {

class SdcardDriver
{
public:
	static sdcard::CardInfo sdcard_info;
	static bool is_initialized;

	~SdcardDriver() = default;
	SdcardDriver() { };
	static sdcard::Result init();
	static sdcard::Result read_block(uint8_t *buffer, uint64_t address);
	static sdcard::Result write_block(uint8_t *buffer, uint64_t address);
	static sdcard::Result read_blocks(uint8_t *buffer, uint64_t address, uint32_t blockcount);
	static sdcard::Result write_blocks(uint8_t *buffer, uint64_t address, uint32_t blockcount);
	static sdcard::Result wait_read_operation(void);
	static sdcard::Result wait_write_operation(void);
	static sdcard::Result erase(uint64_t startaddr, uint64_t endaddr);
	static sdcard::TransferState get_status(void);
	static sdcard::Result sdio_irq_proc(void);
	static void dma_irq_proc(void);
	static uint8_t detect(void);

private:
	static GPIO_CPP_Extension::GPIO_ext _sd_det;

	static SDIO_config _config;
	static SDIO_command _command;
	static SDIO_data_config _data_config;

	static sdcard::Result _power_on(void);
	static sdcard::Result _power_off(void);
	static sdcard::Result _cards_init(void);
	static sdcard::Result _get_card_info(sdcard::CardInfo *info);
	static sdcard::Result _select_deselect(uint64_t address);
	static sdcard::Result _enable_wide_bus_operation(SDIO_WideBusMode mode);
	static sdcard::Result _stop_transfer(void);
	static sdcard::Result _get_card_status(sdcard::CardStatus *status);
	static sdcard::Result _send_sd_status(uint32_t *status);
	static sdcard::Result _send_status(uint32_t *status);
	static sdcard::State _get_state(void);
	static sdcard::Result _cmd_error(void);
	static sdcard::Result _cmd_R1_error(uint8_t cmd);
	static sdcard::Result _cmd_R7_error(void);
	static sdcard::Result _cmd_R3_error(void);
	static sdcard::Result _cmd_R2_error(void);
	static sdcard::Result _cmd_R6_error(uint8_t cmd, uint16_t *rca);
	static sdcard::Result _wide_bus(bool enable);
	static sdcard::Result _is_busy(uint8_t *status);
	static sdcard::Result _find_SCR(uint16_t rca, uint32_t *scr);

	static void _sdio_config();
	static void _dma_tx_config(uint32_t *buffer, uint32_t size);
	static void _dma_rx_config(uint32_t *buffer, uint32_t size);
};

} /* namespace sdcard */

} /* namespace hw */

#endif /* HW_SDCARD_H_ */
