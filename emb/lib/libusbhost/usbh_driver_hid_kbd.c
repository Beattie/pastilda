#include "usbh_core.h"
#include "usbh_device_driver.h"
#include "usbh_driver_hid_kbd.h"

#include <libopencm3/usb/usbstd.h>

#include <stdint.h>
#include "libopencm3/usb/hid.h"

#define USB_HID_SET_REPORT 0x09
#define USB_HID_SET_IDLE 0x0A

enum STATES {
    STATE_INACTIVE,
    STATE_READING_REQUEST,
    STATE_READING_COMPLETE_AND_CHECK_REPORT,
    STATE_SET_REPORT_EMPTY_READ,
    STATE_SET_CONFIGURATION_REQUEST,
    STATE_SET_CONFIGURATION_EMPTY_READ,
    STATE_GET_HID_DESCRIPTOR,// configuration is complete at this point. We write request
    STATE_GET_HID_DESCRIPTOR_RESPONSE_READ,
    STATE_GET_HID_DESCRIPTOR_RESPONSE_READ_COMPLETE_AND_GET_REPORT_DESCRIPTOR,
    STATE_GET_REPORT_DESCRIPTOR_READ,
    STATE_GET_REPORT_DESCRIPTOR_READ_COMPLETE,// after the read finishes, we parse that descriptor
    STATE_SET_IDLE,
    STATE_SET_IDLE_COMPLETE,
};

enum REPORT_STATE {
    REPORT_STATE_NULL,
    REPORT_STATE_READY,
    REPORT_STATE_WRITE_PENDING,
    REPORT_STATE_WRITE_PENDING_DATA,
    REPORT_STATE_EMPTY_READ,
};

struct _hid_kbd_device {
	usbh_device_t *usbh_device;
	uint8_t buffer[USBH_HID_KBD_BUFFER];
	uint16_t endpoint_in_maxpacketsize;
	uint8_t endpoint_in_address;
	enum STATES state_next;
	uint8_t endpoint_in_toggle;
	uint8_t device_id;
	uint8_t configuration_value;
	uint16_t report0_length;
	enum REPORT_STATE report_state;
	uint8_t report_data[USBH_HID_REPORT_BUFFER];
	uint8_t report_data_length;
	enum HID_TYPE hid_type;
	uint8_t interface_number;

};
typedef struct _hid_kbd_device hid_kbd_device_t;

struct hid_report_decriptor {
    struct usb_hid_descriptor header;
    struct _report_descriptor_info {
        uint8_t bDescriptorType;
        uint16_t wDescriptorLength;
    } __attribute__((packed)) report_descriptors_info[];
} __attribute__((packed));

static hid_kbd_device_t kbd_device[USBH_HID_KBD_MAX_DEVICES];
static const hid_kbd_config_t *kbd_config;



static bool initialized = false;

void hid_kbd_driver_init(const hid_kbd_config_t *config)
{
	uint32_t i;
	initialized = true;

	kbd_config = config;
	for (i = 0; i < USBH_HID_KBD_MAX_DEVICES; i++) {
		kbd_device[i].state_next = STATE_INACTIVE;
	}
}

static void *init(void *usbh_dev)
{
	if (!initialized) {
		return (0);
	}

	uint32_t i;
	hid_kbd_device_t *drvdata = 0;

	// find free data space for mouse device
	for (i = 0; i < USBH_HID_KBD_MAX_DEVICES; i++) {
		if (kbd_device[i].state_next == STATE_INACTIVE) {
			drvdata = &kbd_device[i];
			drvdata->device_id = i;
			drvdata->endpoint_in_address = 0;
			drvdata->endpoint_in_toggle = 0;
			drvdata->usbh_device = (usbh_device_t *)usbh_dev;
			drvdata->report_state = REPORT_STATE_NULL;
			drvdata->hid_type = HID_TYPE_NONE;
			break;
		}
	}

	return (drvdata);
}

static void parse_report_descriptor(hid_kbd_device_t *hid, const uint8_t *buffer, uint32_t length)
{
	// TODO
	// Do some parsing!
	// add some checks
	hid->report_state = REPORT_STATE_READY;

	// TODO: parse this from buffer!
	hid->report_data_length = 1;
}

static bool analyze_descriptor(void *drvdata, void *descriptor)
{
	hid_kbd_device_t *hid = (hid_kbd_device_t *)drvdata;
	uint8_t desc_type = ((uint8_t *)descriptor)[1];
	switch (desc_type) {
	case USB_DT_CONFIGURATION:
		{
			const struct usb_config_descriptor * cfg = (const struct usb_config_descriptor*)descriptor;
			hid->configuration_value = cfg->bConfigurationValue;
		}
		break;

	case USB_DT_DEVICE:
		{
			const struct usb_device_descriptor *devDesc = (const struct usb_device_descriptor *)descriptor;
		}
		break;

	case USB_DT_INTERFACE:
		{
			const struct usb_interface_descriptor *ifDesc = (const struct usb_interface_descriptor *)descriptor;
			if (ifDesc->bInterfaceProtocol == 0x01) {
				hid->hid_type = HID_TYPE_KEYBOARD;
				hid->interface_number = ifDesc->bInterfaceNumber;
			} else if (ifDesc->bInterfaceProtocol == 0x02) {
				hid->hid_type = HID_TYPE_MOUSE;
				hid->interface_number = ifDesc->bInterfaceNumber;
			}
		}
		break;

	case USB_DT_ENDPOINT:
		{
			const struct usb_endpoint_descriptor *ep = (const struct usb_endpoint_descriptor*)descriptor;
			if ((ep->bmAttributes&0x03) == USB_ENDPOINT_ATTR_INTERRUPT) {
				uint8_t epaddr = ep->bEndpointAddress;
				if (epaddr & (1<<7)) {
					hid->endpoint_in_address = epaddr&0x7f;
					if (ep->wMaxPacketSize < USBH_HID_KBD_BUFFER) {
						hid->endpoint_in_maxpacketsize = ep->wMaxPacketSize;
					} else {
						hid->endpoint_in_maxpacketsize = USBH_HID_KBD_BUFFER;
					}
				}

				if (hid->endpoint_in_address) {
					hid->state_next = STATE_SET_CONFIGURATION_REQUEST;
					return true;
				}
			}
		}
		break;

	default:
		break;
	}
	return false;
}

static void report_event(usbh_device_t *dev, usbh_packet_callback_data_t cb_data)
{
	hid_kbd_device_t *hid = (hid_kbd_device_t *)dev->drvdata;
	switch (hid->report_state) {
	case REPORT_STATE_WRITE_PENDING:
		switch (cb_data.status) {
		case USBH_PACKET_CALLBACK_STATUS_OK:
			hid->report_state = REPORT_STATE_WRITE_PENDING_DATA;

			device_xfer_control_write_data(hid->report_data, hid->report_data_length, report_event, dev);
			break;

		case USBH_PACKET_CALLBACK_STATUS_ERRSIZ:
		case USBH_PACKET_CALLBACK_STATUS_EFATAL:
		case USBH_PACKET_CALLBACK_STATUS_EAGAIN:
			//ERROR(cb_data.status);
//			hid->state_next = STATE_INACTIVE; // WHAT to do here?
			break;
		}
		break;

	case REPORT_STATE_WRITE_PENDING_DATA:
		switch (cb_data.status) {
		case USBH_PACKET_CALLBACK_STATUS_OK:
			hid->report_state = REPORT_STATE_EMPTY_READ;
			device_xfer_control_read(0, 0, report_event, dev);
			//LOG_PRINTF("reading empty\n");
			break;

		case USBH_PACKET_CALLBACK_STATUS_ERRSIZ:
		case USBH_PACKET_CALLBACK_STATUS_EFATAL:
		case USBH_PACKET_CALLBACK_STATUS_EAGAIN:
			//ERROR(cb_data.status);
//			hid->state_next = STATE_INACTIVE; // WHAT to do here?
			break;
		}
		break;


	case REPORT_STATE_EMPTY_READ:
		switch (cb_data.status) {
		case USBH_PACKET_CALLBACK_STATUS_OK:
			hid->report_state = REPORT_STATE_READY;
			break;

		case USBH_PACKET_CALLBACK_STATUS_ERRSIZ:
		case USBH_PACKET_CALLBACK_STATUS_EFATAL:
		case USBH_PACKET_CALLBACK_STATUS_EAGAIN:
			//ERROR(cb_data.status);
//			hid->state_next = STATE_INACTIVE; // WHAT to do here?
			break;
		}
		break;
	default:
		break;
	}
}


static void event(usbh_device_t *dev, usbh_packet_callback_data_t cb_data)
{
	hid_kbd_device_t *hid = (hid_kbd_device_t *)dev->drvdata;

	switch (hid->state_next) {
	case STATE_READING_COMPLETE_AND_CHECK_REPORT:
		{
			switch (cb_data.status) {
			case USBH_PACKET_CALLBACK_STATUS_OK:
			case USBH_PACKET_CALLBACK_STATUS_ERRSIZ:
				if (kbd_config->kbd_in_message_handler) {
					kbd_config->kbd_in_message_handler(hid->device_id, hid->buffer, cb_data.transferred_length);
				}
				hid->state_next = STATE_READING_REQUEST;
				break;

			case USBH_PACKET_CALLBACK_STATUS_EFATAL:
			case USBH_PACKET_CALLBACK_STATUS_EAGAIN:
				//ERROR(cb_data.status);
				hid->state_next = STATE_INACTIVE;
				break;
			}
		}
		break;

	case STATE_SET_CONFIGURATION_EMPTY_READ:
		{
			//LOG_PRINTF("|empty packet read|");
			switch (cb_data.status) {
			case USBH_PACKET_CALLBACK_STATUS_OK:
				hid->state_next = STATE_GET_HID_DESCRIPTOR;
				device_xfer_control_read(0, 0, event, dev);
				break;

			case USBH_PACKET_CALLBACK_STATUS_ERRSIZ:
			case USBH_PACKET_CALLBACK_STATUS_EFATAL:
			case USBH_PACKET_CALLBACK_STATUS_EAGAIN:
				//ERROR(cb_data.status);
				hid->state_next = STATE_INACTIVE;
				break;
			}
		}
		break;

	case STATE_GET_HID_DESCRIPTOR: // Configured. We now get HID descriptor to be able to control leds, etc...
		{
			switch (cb_data.status) {
			case USBH_PACKET_CALLBACK_STATUS_OK:
				hid->endpoint_in_toggle = 0;
				//LOG_PRINTF("\nHID device CONFIGURED. Getting HID descriptor\n");

				struct usb_setup_data setup_data;

				setup_data.bmRequestType = USB_REQ_TYPE_IN | USB_REQ_TYPE_INTERFACE;
				setup_data.bRequest = USB_REQ_GET_DESCRIPTOR;
				setup_data.wValue = USB_DT_HID << 8;
				setup_data.wIndex = 0;
				// we try to read buffer max bytes
				// It will result in ERRSIZ error when reading this many bytes.
				// ERRSIZ, however would not be treated as an error, but instead as a success.
				setup_data.wLength = USBH_HID_KBD_BUFFER;

				hid->state_next = STATE_GET_HID_DESCRIPTOR_RESPONSE_READ;
				device_xfer_control_write_setup(&setup_data, sizeof(setup_data), event, dev);

				break;

			case USBH_PACKET_CALLBACK_STATUS_ERRSIZ:
			case USBH_PACKET_CALLBACK_STATUS_EFATAL:
			case USBH_PACKET_CALLBACK_STATUS_EAGAIN:
				//ERROR(cb_data.status);
				hid->state_next = STATE_INACTIVE;
				break;
			}
		}
		break;

	case STATE_GET_HID_DESCRIPTOR_RESPONSE_READ:
		{
			switch (cb_data.status) {
			case USBH_PACKET_CALLBACK_STATUS_OK:
				hid->state_next = STATE_GET_HID_DESCRIPTOR_RESPONSE_READ_COMPLETE_AND_GET_REPORT_DESCRIPTOR;
				device_xfer_control_read(hid->buffer, USBH_HID_KBD_BUFFER, event, dev);
				break;

			case USBH_PACKET_CALLBACK_STATUS_ERRSIZ:
			case USBH_PACKET_CALLBACK_STATUS_EFATAL:
			case USBH_PACKET_CALLBACK_STATUS_EAGAIN:
				//ERROR(cb_data.status);
				hid->state_next = STATE_INACTIVE;
				break;
			}
		}
		break;


	case STATE_GET_HID_DESCRIPTOR_RESPONSE_READ_COMPLETE_AND_GET_REPORT_DESCRIPTOR:
		{
			switch (cb_data.status) {
			case USBH_PACKET_CALLBACK_STATUS_OK:
			case USBH_PACKET_CALLBACK_STATUS_ERRSIZ:
				{
					hid->state_next = STATE_READING_REQUEST;
					hid->endpoint_in_toggle = 0;
					const struct hid_report_decriptor *hid_descriptor = (const struct hid_report_decriptor *)hid->buffer;
					//LOG_PRINTF("\nRead of HID descriptor complete. Length: %d %d\n", cb_data.transferred_length, hid_descriptor->header.bLength);

					// usb_hid_descriptor(6) and info(3: minimal because one report descriptor is mandatory)
					if (cb_data.transferred_length < sizeof(struct _report_descriptor_info) + sizeof(struct usb_hid_descriptor)) {
						//LOG_PRINTF("Wrong descriptor received %d\n", sizeof(struct _report_descriptor_info) + sizeof(struct usb_hid_descriptor));
						//ERROR(cb_data.status);
						hid->state_next = STATE_INACTIVE;
						break;
					}

					// We support only the first report descriptor with index 0
					uint16_t length_to_read = hid_descriptor->report_descriptors_info[0].wDescriptorLength;

					// limit the size of the report descriptor!
					if (length_to_read > USBH_HID_KBD_BUFFER) {
						length_to_read = USBH_HID_KBD_BUFFER;
					}

					struct usb_setup_data setup_data;

					setup_data.bmRequestType = USB_REQ_TYPE_IN | USB_REQ_TYPE_INTERFACE;
					setup_data.bRequest = USB_REQ_GET_DESCRIPTOR;
					setup_data.wValue = USB_DT_REPORT << 8;
					setup_data.wIndex = 0;
					setup_data.wLength = length_to_read;

					hid->report0_length = length_to_read;

					hid->state_next = STATE_GET_REPORT_DESCRIPTOR_READ;
					device_xfer_control_write_setup(&setup_data, sizeof(setup_data), event, dev);

				}
				break;

			case USBH_PACKET_CALLBACK_STATUS_EFATAL:
			case USBH_PACKET_CALLBACK_STATUS_EAGAIN:
				//ERROR(cb_data.status);
				hid->state_next = STATE_INACTIVE;
				break;
			}
		}
		break;

	case STATE_GET_REPORT_DESCRIPTOR_READ:
		{
			switch (cb_data.status) {
			case USBH_PACKET_CALLBACK_STATUS_OK:
				hid->state_next = STATE_GET_REPORT_DESCRIPTOR_READ_COMPLETE;
				device_xfer_control_read(hid->buffer, hid->report0_length, event, dev);
				break;

			case USBH_PACKET_CALLBACK_STATUS_ERRSIZ:
			case USBH_PACKET_CALLBACK_STATUS_EFATAL:
			case USBH_PACKET_CALLBACK_STATUS_EAGAIN:
				//ERROR(cb_data.status);
				hid->state_next = STATE_INACTIVE;
				break;
			}
		}
		break;


	case STATE_GET_REPORT_DESCRIPTOR_READ_COMPLETE: // read complete, SET_IDLE to 0
		{
			switch (cb_data.status) {
			case USBH_PACKET_CALLBACK_STATUS_OK:
				//LOG_PRINTF("READ REPORT COMPLETE \n");
				hid->state_next = STATE_READING_REQUEST;
				hid->endpoint_in_toggle = 0;

				parse_report_descriptor(hid, hid->buffer, cb_data.transferred_length);
				break;

			case USBH_PACKET_CALLBACK_STATUS_ERRSIZ:
			case USBH_PACKET_CALLBACK_STATUS_EFATAL:
			case USBH_PACKET_CALLBACK_STATUS_EAGAIN:
				//ERROR(cb_data.status);
				hid->state_next = STATE_INACTIVE;
				break;
			}
		}
		break;

	default:
		break;
	}
}

static void read_kbd_in(void *drvdata)
{
	hid_kbd_device_t *hid = (hid_kbd_device_t *)drvdata;
	usbh_packet_t packet;

	packet.address = hid->usbh_device->address;
	packet.data = &hid->buffer[0];
	packet.datalen = hid->endpoint_in_maxpacketsize;
	packet.endpoint_address = hid->endpoint_in_address;
	packet.endpoint_size_max = hid->endpoint_in_maxpacketsize;
	packet.endpoint_type = USBH_ENDPOINT_TYPE_INTERRUPT;
	packet.speed = hid->usbh_device->speed;
	packet.callback = event;
	packet.callback_arg = hid->usbh_device;
	packet.toggle = &hid->endpoint_in_toggle;

	hid->state_next = STATE_READING_COMPLETE_AND_CHECK_REPORT;
	usbh_read(hid->usbh_device, &packet);

	// LOG_PRINTF("@HID EP1 |  \n");
}

static void poll(void *drvdata, uint32_t time_curr_us)
{
	(void)time_curr_us;

	hid_kbd_device_t *kbd = (hid_kbd_device_t *)drvdata;
	usbh_device_t *dev = kbd->usbh_device;
	switch (kbd->state_next) {
	case STATE_READING_REQUEST:
		{
			read_kbd_in(drvdata);
		}
		break;

	case STATE_SET_CONFIGURATION_REQUEST:
		{
			struct usb_setup_data setup_data;

			setup_data.bmRequestType = 0b00000000;
			setup_data.bRequest = USB_REQ_SET_CONFIGURATION;
			setup_data.wValue = kbd->configuration_value;
			setup_data.wIndex = 0;
			setup_data.wLength = 0;

			kbd->state_next = STATE_SET_CONFIGURATION_EMPTY_READ;

			device_xfer_control_write_setup(&setup_data, sizeof(setup_data), event, dev);
		}
		break;

	default:
		// do nothing - probably transfer is in progress
		break;
	}
}

static void remove(void *drvdata)
{
	hid_kbd_device_t *kbd = (hid_kbd_device_t *)drvdata;
	kbd->state_next = STATE_INACTIVE;
	kbd->endpoint_in_address = 0;
}



bool hid_set_report(uint8_t device_id, uint8_t val)
{
    if (device_id >= USBH_HID_KBD_MAX_DEVICES) {
        //LOG_PRINTF("invalid device id");
        return false;
    }

    hid_kbd_device_t *hid = &kbd_device[device_id];
    if (hid->report_state != REPORT_STATE_READY) {
        //LOG_PRINTF("reporting is not ready\n");
        // store and update afterwards
        return false;
    }

    if (hid->report_data_length == 0) {
        //LOG_PRINTF("reporting is not available (report len=0)\n");
        return false;
    }

    struct usb_setup_data setup_data;
    setup_data.bmRequestType = USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE;
    setup_data.bRequest = USB_HID_SET_REPORT;
    setup_data.wValue = 0x02 << 8;
    setup_data.wIndex = hid->interface_number;
    setup_data.wLength = hid->report_data_length;

    hid->report_data[0] = val;

    hid->report_state = REPORT_STATE_WRITE_PENDING;
    device_xfer_control_write_setup(&setup_data, sizeof(setup_data), report_event, hid->usbh_device);
    return true;
}

bool is_hid_ready_for_set_report(uint8_t device_id)
{
	return( kbd_device[device_id].report_state == REPORT_STATE_READY );
}


static const usbh_dev_driver_info_t driver_info = {
	.deviceClass = -1,
	.deviceSubClass = -1,
	.deviceProtocol = -1,
	.idVendor = -1,
	.idProduct = -1,
	.ifaceClass = 0x03,//-1, //HID
	.ifaceSubClass = -1, //-1
	.ifaceProtocol = 0x01 //Kbd //0x02
};

const usbh_dev_driver_t usbh_hid_kbd_driver = {
	.init = init,
	.analyze_descriptor = analyze_descriptor,
	.poll = poll,
	.remove = remove,
	.info = &driver_info
};
