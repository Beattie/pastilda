#!/usr/bin/env python3
""" Script for flashing stm32 device """

import subprocess as sp
import sys
import re
import glob
import hashlib
import time
import datetime


def sha256_checksum(filename, block_size=65536):
    """ Find sha256 for binary file
        Source: https://gist.github.com/rji/b38c7238128edf53a181 """
    sha256 = hashlib.sha256()
    with open(filename, 'rb') as file:
        for block in iter(lambda: file.read(block_size), b''):
            sha256.update(block)
    return sha256.hexdigest()


def rm_files(files):
    """ Try to rm files. """
    if files:
        sp.check_call(["rm"] + files, cwd=sys.path[0])


def rm_files_if_exist(file_name):
    """ Try to rm files if it exist """
    files = glob.glob(sys.path[0] + "/" + file_name)
    rm_files(files)


class Error(Exception):
    """ Base class for exceptions in this module. """
    pass


class AppError(Error):
    """ Application error class. """

    def __init__(self, source, message):
        super(AppError, self).__init__()
        self.source = source
        self.message = message


class Logger:
    """ Log something """

    DEBUG_PREFIX = "[DEBUG]"
    WARNING_PREFIX = "[WARNING]"
    ERROR_PREFIX = "[ERROR]"

    @staticmethod
    def log(source, status):
        """ Log command """
        print("{} \t {} status: {}".format(datetime.datetime.utcnow(),
                                           source, status))

    @staticmethod
    def warning_log(source, status):
        """ Warning log command """
        Logger.log("{} {}".format(Logger.WARNING_PREFIX, source), status)

    @staticmethod
    def error_log(source, status):
        """ Error log command """
        Logger.log("{} {}".format(Logger.ERROR_PREFIX, source), status)

    @staticmethod
    def debug_log(source, status):
        """ Debug log command """
        Logger.log("{} {}".format(Logger.DEBUG_PREFIX, source), status)


class DfuUtil:
    """ Dfu util class """

    DFU_UTIL_LAST_VER = "dfu-util 0.9"
    DFU_UTIL_COMMAD = "dfu-util"
    RESP_GARBAGE_STRING_NUM_END = 7

    STM32_DEV_ALT_NUM = "-a 0"

    RE_VENDOR_STUFF = '([a-zA-Z0-9]{4}:[a-zA-Z0-9]{4})'
    RE_ANOTHER_STUFF = '(.+)="?([^",]+)(",)*'

    TEMP_LEAVING_FILE_NAME = 'temp_leaving_file.bin'

    def __init__(self):
        self._dev_list = None
        self._error_occurred = False
        try:
            self.check_dfu_util()
        except AppError as err:
            raise err
        Logger.log("dfu-util", "dfu util OK.")

    def check_dfu_util(self):
        """ Check if dfu-util exist in OS """
        try:
            version = self.version()
        except AppError as err:
            raise err

        if version != DfuUtil.DFU_UTIL_LAST_VER:
            Logger.warning_log("dfu-util",
                               "You have got not the " +
                               "latest version of dfu-util")

    def version(self):
        """ Get actual dfu-util version """
        return self.get_clean_response("-V")[0]

    def dev_list(self):
        """ Get list of devs in DFU mode """
        dev_list = self.get_clean_response("--list")
        if not dev_list:
            raise AppError("dfu-utils", "No DFU devices found!")

        return dev_list

    def dev_list_parsed(self):
        """ Get list of devs in DFU mode parsed in list of dicts """
        try:
            dev_list = self.dev_list()
        except AppError as err:
            raise err

        parsed_list = []

        for dev in dev_list:
            dev_splitted = dev.split(" ")[2:]

            dev_attr_list = []
            dev_attr_val_list = []
            for dev_attr in dev_splitted:
                res = re.search(DfuUtil.RE_VENDOR_STUFF, dev_attr)
                if res:
                    dev_attr_list.append("dev")
                    dev_attr_val_list.append(res.group(1))
                    continue

                res = re.search(DfuUtil.RE_ANOTHER_STUFF, dev_attr)
                if res:
                    dev_attr_list.append(res.group(1))
                    dev_attr_val_list.append(res.group(2))
                    continue

            parsed_list.append(dict(zip(dev_attr_list, dev_attr_val_list)))

        return parsed_list

    def flash_device(self, args, path_to_dfu):
        """ Flash DFU file on device """
        try:
            return self.get_clean_response(args + " -D " + path_to_dfu)
        except AppError as err:
            raise err

    def flash_device_as_subproc(self, args, path_to_dfu):
        """ Flash DFU file on device (start as subproc) """
        try:
            return self.start_subproc(args + " -D " + path_to_dfu)
        except AppError as err:
            raise err

    def get_flash_from_device(self, args, size, output_path):
        """ Get copy of dev mem """
        try:
            self.start_subproc("{} -s 0x08000000:{} -U {}"
                               .format(args, size, output_path))
        except AppError as err:
            raise err

    def leave_dfu_mode(self, args):
        """ Ask device to leave DFU mode (for dfuse only) """

        rm_files_if_exist(DfuUtil.TEMP_LEAVING_FILE_NAME)

        try:
            self.get_response("{} -s 0x08000000:{} -U {}"
                              .format(args, "leave",
                                      DfuUtil.TEMP_LEAVING_FILE_NAME))
            rm_files([DfuUtil.TEMP_LEAVING_FILE_NAME])
        except AppError as err:
            raise err
        except sp.CalledProcessError:
            Logger.warning_log("dfu-util", "Can't remove temp_leaving_file")

    @staticmethod
    def get_response(args):
        """ Eval command dfu with args """
        args_splitted = args.split(" ")
        output = None
        try:
            output = sp.check_output([DfuUtil.DFU_UTIL_COMMAD] + args_splitted,
                                     cwd=sys.path[0],
                                     stderr=sp.STDOUT)
            return str(output, 'utf-8')
        except sp.CalledProcessError as err:
            raise AppError("dfu-util", "ERROR\n\n" + str(err.output, 'utf-8'))
        except FileNotFoundError:
            raise AppError("dfu-util", "{} not found"
                           .format(DfuUtil.DFU_UTIL_COMMAD))

    def get_clean_response(self, args):
        """ Eval command dfu with args and return only useful stuff """
        try:
            resp = self.get_response(args)
        except AppError as err:
            raise err

        resp_splitted = resp.split("\n")
        if args == "-V":
            return [resp_splitted[0]]

        if resp_splitted[DfuUtil.RESP_GARBAGE_STRING_NUM_END] != '':
            return resp_splitted[DfuUtil.RESP_GARBAGE_STRING_NUM_END:-1]

        return []

    @staticmethod
    def start_subproc(args):
        """ Start command as subproc and redirect output """
        args_splitted = args.split(" ")
        try:
            sp.check_call([DfuUtil.DFU_UTIL_COMMAD] + args_splitted,
                          cwd=sys.path[0],
                          stderr=sp.STDOUT)
        except sp.CalledProcessError as err:
            raise AppError("dfu-util", "ERROR\n\n" + str(err.output, 'utf-8'))


class Flasher:
    """ Class for flashing dfu files on STM32 devices """

    CONVERTED_BIN_FILE_EXT = ".target0.image0.bin"
    ACTUAL_BIN_FILE_EXT = ".actual.bin"

    LEAVING_DFU_SLEEP_TIME = 1  # sec

    def __init__(self, dfu_tool):
        self._dfu = dfu_tool
        self._dev_list = None

    def flash(self, path_to_dfu):
        """ Main flash method """
        Logger.log("Flashing", "Flashing start.")

        try:
            self.get_dev_list()
            self.find_stm32_device()
            self.flash_dev(path_to_dfu)
            self.test_dfu(path_to_dfu)

            Logger.log("Flashing", "Flashing process ended successfully!")

            self.leaving_dfu()
        except AppError as err:
            raise err

    def get_dev_list(self):
        """ Get list of DFU ready devices """
        try:
            self._dev_list = self._dfu.dev_list_parsed()
        except AppError as err:
            raise err

    def find_stm32_device(self):
        """ Check if some STM32 devices exist """
        is_dev_found = False
        for dev in self._dev_list:
            if dev["alt"] == "0":
                Logger.log("Flashing", "STM32 device found.")
                is_dev_found = True

        if not is_dev_found:
            raise AppError("Flasher", "STM32 device not found!")

    def flash_dev(self, path_to_dfu):
        """ Flash device with DFU file """
        Logger.log("Flashing", "Flashing process start")
        try:
            self._dfu.flash_device_as_subproc(DfuUtil.STM32_DEV_ALT_NUM,
                                              path_to_dfu)
        except AppError as err:
            raise err
        Logger.log("Flashing", "Flashing process end")

    def test_dfu(self, path_to_dfu):
        """ Get bin from device and compare with DFU file """
        Logger.log("Flashing", "Getting mem copy")

        # Remove old bin files if they exits
        rm_files_if_exist(path_to_dfu + ".*")

        # Make bin from dfu
        sp.check_call(["python", "./dfu-convert", "-d", path_to_dfu],
                      cwd=sys.path[0])

        # Get mem copy
        try:
            size_of_bin = str(sp.check_output(["wc", "-c",
                                               path_to_dfu +
                                               Flasher.CONVERTED_BIN_FILE_EXT],
                                              cwd=sys.path[0]),
                              'utf-8').split(" ")[0]
            self._dfu.get_flash_from_device(DfuUtil.STM32_DEV_ALT_NUM,
                                            size_of_bin, path_to_dfu +
                                            Flasher.ACTUAL_BIN_FILE_EXT)
        except AppError as err:
            raise err

        # Calc sha256
        right_dfu_file_sha256 = sha256_checksum(sys.path[0] + '/' +
                                                path_to_dfu +
                                                Flasher.CONVERTED_BIN_FILE_EXT)
        Logger.log("Flashing", "dfu sha265:\t\t{}"
                   .format(right_dfu_file_sha256))

        actual_dfu_file_sha256 = sha256_checksum(sys.path[0] +
                                                 '/' +
                                                 path_to_dfu +
                                                 Flasher.ACTUAL_BIN_FILE_EXT)

        Logger.log("Flashing", "dfu on dev sha265:\t{}"
                   .format(actual_dfu_file_sha256))

        if right_dfu_file_sha256 != actual_dfu_file_sha256:
            raise AppError("Flasher",
                           "sha256s not equal! Something gone wrong.")

        # Remove old bin files if they exits
        rm_files_if_exist(path_to_dfu + ".*")

    def leaving_dfu(self):
        """ Trying to leave DFU mode """
        Logger.log("Flashing", "Leaving DFU mode...")
        try:
            self._dfu.leave_dfu_mode(DfuUtil.STM32_DEV_ALT_NUM)
        except AppError as err:
            raise err

        time.sleep(Flasher.LEAVING_DFU_SLEEP_TIME)

        try:
            self._dfu.dev_list_parsed()
            Logger.warning_log("Flashing",
                               "​Device has not been reset yet, " +
                               "try to reset it by yourself")
        except AppError:
            Logger.log("Flashing", "Device successfully reset!")


def __main():
    """ App entry point """

    try:
        name_of_dfu_file = sys.argv[1]
    except IndexError:
        name_of_dfu_file = "boot.dfu"
        Logger.warning_log("Script", "Path to dfu file was not set." +
                           "Using default path: 'boot.dfu'")

    # Check dfu-tools
    try:
        dfu_util = DfuUtil()
    except AppError as err:
        Logger.error_log(err.source, err.message)
        Logger.error_log("\n\nScript", "FAILED")
        sys.exit(1)

    # Start flashing
    flasher = Flasher(dfu_util)

    try:
        flasher.flash(name_of_dfu_file)
    except AppError as err:
        Logger.log(err.source, err.message)
        Logger.error_log("\n\nScript", "FAILED")
        sys.exit(1)

    Logger.log("\n\nScript", "FINISHED SUCCESSFULLY")
    sys.exit()


if __name__ == "__main__":
    __main()
