#!/usr/bin/env sh
IFS=$'\n\t'

# Trying to know what version of Python has been set by default
default_python_ver=$(python -V 2>&1)

default_python_ver_formated="$(echo $default_python_ver | cut -c-8)"
echo $default_python_ver_formated "was detected used by default!"

# Get only the Python first version number and check if it equal to "3"
if [ "$(echo $default_python_ver | cut -c 8)" = "3" ]
then
    python flash_dev.py "boot.dfu"
else
# If python3 was not set by default then try to start it explicitly
    echo "Starting script using command python3..."
    python3 flash_dev.py "boot.dfu"
fi
