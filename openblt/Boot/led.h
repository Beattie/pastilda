/************************************************************************************//**
* \file         Demo\ARMCM3_STM32F2_Olimex_STM32P207_GCC\Boot\led.h
* \brief        LED driver header file.
* \ingroup      Boot_ARMCM3_STM32F2_Olimex_STM32F207_GCC
* \internal
*----------------------------------------------------------------------------------------
*                          C O P Y R I G H T
*----------------------------------------------------------------------------------------
*   Copyright (c) 2016  by Feaser    http://www.feaser.com    All rights reserved
*
*----------------------------------------------------------------------------------------
*                            L I C E N S E
*----------------------------------------------------------------------------------------
* This file is part of OpenBLT. OpenBLT is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 3 of the License, or (at your option) any later
* version.
*
* OpenBLT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
* PURPOSE. See the GNU General Public License for more details.
*
* You have received a copy of the GNU General Public License along with OpenBLT. It
* should be located in ".\Doc\license.html". If not, contact Feaser to obtain a copy.
*
* \endinternal
****************************************************************************************/
#ifndef LED_H
#define LED_H

#include "stm32f2xx.h"                           /* STM32 registers and drivers        */

#define USART_PORT  GPIOA
#define LED_PORT    GPIOA
#define RED_LED     GPIO_Pin_1
#define GREEN_LED   GPIO_Pin_0
#define BLUE_LED    GPIO_Pin_2
#define YELLOW_LED  RED_LED | GREEN_LED
#define MAGENTA_LED RED_LED | BLUE_LED
#define CYAN_LED    GREEN_LED | BLUE_LED
#define WHITE_LED   RED_LED | GREEN_LED | BLUE_LED

extern uint16_t active_led;

/****************************************************************************************
* Function prototypes
****************************************************************************************/
void LedBlinkInit(blt_int16u interval_ms);
void LedBlinkTask(void);
void LedBlinkExit(void);


#endif /* LED_H */
/*********************************** end of led.h **************************************/
